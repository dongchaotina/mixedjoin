#include "../Common/stdafx.h"
#include "../BinaryJson/json.h"
#include "../Common/Types.h"
#include "../Internal/Element/Element.h"
#include "../Query/QueryIntermediateRepresentation.h"
#include "../Server/JsonStreamServer.h"
#include "../Schema/JsonSchema.h"
#include "../IO/SocketStreamOutput.h"
#include "../IO/IStreamOutput.h"
#include "../Parser/ParserManager.h"
#include <boost/asio.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/regex.hpp>
#include <iostream>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#ifdef PROFILER
#include  <google/profiler.h>
#endif

int main() {
#ifdef DEBUG
	//test();
#endif
	//test();
	//start server

#ifdef PROFILER
	ProfilerStart("MyProfile.prof");
#endif
	JsonStreamServer* server = JsonStreamServer::getInstance();

/*	string jaqlQueryString =
			"stream1 = readFromWrapper (\"performanceTestStream1\", true) ;\
		             tmpv = stream1 -> window[rows 100] ;\
		             tmpv -> istream;";
	boost::shared_ptr<QueryIntermediateRepresentation> queryIntermediateRepresentation =
			ParserManager::getInstance()->processQuery(jaqlQueryString);

	cout << queryIntermediateRepresentation->getDocument() << endl;*/

	//string queryIntermediateString =
	//		"{type:\"root\",input:{type:\"istream\",input:{type:\"join\",left_join_attribute:[\"A\"],right_join_attribute:[\"A\"],left_outer:false,right_outer:false,projection:{type:\"projection_obj\",projection_type:\"object\",fields:[{type:\"projection_obj\",need_rename:false,projection_type:\"direct\",expression:{type:\"expression_obj\",expression_type:\"id\",id_name:[\"A\"],attribute_source:\"left\"}},{type:\"projection_obj\",need_rename:false,projection_type:\"direct\",expression:{type:\"expression_obj\",expression_type:\"id\",id_name:[\"PK\"],attribute_source:\"right\"}}]},left_input:{type:\"rowwindow\",windowsize:2,input:{type:\"leaf\",stream_source:\"performanceTestStream1\",is_master:false}},right_input:{type:\"rowwindow\",windowsize:10000,input:{type:\"leaf\",stream_source:\"performanceTestStream1\",is_master:false}}}}}";
	//string queryIntermediateString = "{\"type\":\"root\",\"input\":{\"type\":\"istream\",\"input\":{\"type\":\"join\",\"left_join_attribute\":[\"A\"],\"right_join_attribute\":[\"A\"],\"left_outer\":false,\"right_outer\":false,\"projection\":{\"type\":\"projection_obj\",\"projection_type\":\"object\",\"fields\":[{\"type\":\"projection_obj\",\"need_rename\":false,\"projection_type\":\"direct\",\"expression\":{\"type\":\"expression_obj\",\"expression_type\":\"id\",\"id_name\":[\"A\"],\"attribute_source\":\"left\"}},{\"type\":\"projection_obj\",\"need_rename\":false,\"projection_type\":\"direct\",\"expression\":{\"type\":\"expression_obj\",\"expression_type\":\"id\",\"id_name\":[\"PK\"],\"attribute_source\":\"right\"}},{\"type\":\"projection_obj\",\"need_rename\":false,\"projection_type\":\"direct\",\"expression\":{\"type\":\"expression_obj\",\"expression_type\":\"id\",\"id_name\":[\"A\"],\"attribute_source\":\"right\"}}]},\"left_input\":{\"type\":\"rowwindow\",\"windowsize\":100,\"input\":{\"type\":\"leaf\",\"stream_source\":\"performanceTestStream1\",\"is_master\":false}},\"right_input\":{\"type\":\"relation_leaf\",\"table_name\":\"test\",\"table_source\":\"relation1\",\"is_master\":false}}}}";
	string queryIntermediateString = "{\"type\":\"root\",\"input\":{\"type\":\"istream\",\"input\":{\"type\":\"join\",\"left_join_attribute\":[\"work\"],\"right_join_attribute\":[\"A\"],\"left_outer\":false,\"right_outer\":false,\"projection\":{\"type\":\"projection_obj\",\"projection_type\":\"object\",\"fields\":[{\"type\":\"projection_obj\",\"need_rename\":false,\"projection_type\":\"direct\",\"expression\":{\"type\":\"expression_obj\",\"expression_type\":\"id\",\"id_name\":[\"work\"],\"attribute_source\":\"left\"}},{\"type\":\"projection_obj\",\"need_rename\":false,\"projection_type\":\"direct\",\"expression\":{\"type\":\"expression_obj\",\"expression_type\":\"id\",\"id_name\":[\"PK\"],\"attribute_source\":\"right\"}},{\"type\":\"projection_obj\",\"need_rename\":false,\"projection_type\":\"direct\",\"expression\":{\"type\":\"expression_obj\",\"expression_type\":\"id\",\"id_name\":[\"A\"],\"attribute_source\":\"right\"}}]},\"left_input\":{\"type\":\"rowwindow\",\"windowsize\":2,\"input\":{\"type\":\"leaf\",\"stream_source\":\"tokyoPeopleFlowStreamShort\",\"is_master\":false}},\"right_input\":{\"type\":\"relation_leaf\",\"table_name\":\"test\",\"table_source\":\"relation1\",\"is_master\":false}}}}";


	/*	std::string jaqlQueryString =
	 "stream1 = readFromWrapper (\"performanceTestStream1\", true) ;\
	stream2 = readFromWrapper (\"performanceTestStream1\", false) ;\
	tmp1 = stream1 -> window[rows 100] ;\
	tmp2 = stream2 -> window[rows 100] ; \
	j = join s in tmp1,\
           	d in tmp2\
		where s.A == d.A\
		into {s.A, d.B};\
	j -> istream;";
	 boost::shared_ptr<QueryIntermediateRepresentation> queryIntermediateRepresentation =
	 ParserManager::getInstance()->processQuery(jaqlQueryString);
	 cout << queryIntermediateRepresentation->getDocument() << endl;*/
	Document queryIntermediateDocument = fromjson(queryIntermediateString);
	boost::shared_ptr<QueryIntermediateRepresentation> queryIntermediateRepresentation(
			new QueryIntermediateRepresentation(queryIntermediateDocument));

	boost::shared_ptr<IStreamOutput> streamOutput3(
			new ShowResultStreamOutput());
	std::string durationSpecification3 = "0s";
	server->registerQuery(queryIntermediateRepresentation, streamOutput3,
			durationSpecification3);

	server->execute();    //exeute the query
	std::cout << "exist the system" << std::endl;

#ifdef PROFILER
	ProfilerStop();
#endif
}

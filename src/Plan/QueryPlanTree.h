#pragma once
#include "../Common/stdafx.h"
#include "../Operator/Operator.h"
class QueryPlanTree
{
private:
	std::list<boost::shared_ptr<Operator> >operatorList; 
	void addOperator(boost::shared_ptr<Operator> op,std::list<boost::shared_ptr<Operator> >&operatorList);
public:
	QueryPlanTree(boost::shared_ptr<Operator>rootOperator);
	~QueryPlanTree();
	std::list<boost::shared_ptr<Operator> >getAllOperators();
	void setOperatorIdAndQueueId(std::string prefix); 
};


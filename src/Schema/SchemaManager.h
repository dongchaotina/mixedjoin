#pragma once
#include "../Common/stdafx.h"
#include <boost/noncopyable.hpp>
#include "../Schema/JsonSchema.h"
#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>  
/* manage all the schemas registered in the system */
class SchemaManager: private boost::noncopyable
{
private:
	static SchemaManager * schemaManager;
	SchemaManager(void);
	boost::unordered_map<std::string, boost::shared_ptr<JsonSchema> >schemaMap;
public:
	static SchemaManager * getInstance(void);
	~SchemaManager(void);
	void registerJsonSchema(boost::shared_ptr<JsonSchema> jsonSchema);
	bool getJsonSchemaById(std::string id, boost::shared_ptr<JsonSchema>& jsonSchema);
};


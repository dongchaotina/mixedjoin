#include "../Common/stdafx.h"
#include "../Scheduler/Scheduler.h"
#include "../Operator/LeafOperator.h"
#include "../Operator/RootOperator.h"
#include "../Operator/RelationLeafOperator.h"
#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/any.hpp>
Scheduler* Scheduler::scheduler = NULL;
Scheduler::Scheduler() {

}

Scheduler::~Scheduler(void) {
}
Scheduler* Scheduler::getInstance() {
	if (scheduler == NULL) {
		scheduler = new Scheduler();
	}
	return scheduler;
}

void Scheduler::getNextOperatorToExecute(boost::shared_ptr<Operator> & op) {

	boost::mutex::scoped_lock lk(registeOperatorListMutex);
	if (it == this->operatorList.end()) {
		it = this->operatorList.begin();
	}
	op = *it;
	it++;
	return;

}

void Scheduler::checkRelationLeafOperator() {
	boost::mutex::scoped_lock lk(registeOperatorListMutex);
	for (it == this->operatorList.begin(); it != this->operatorList.end();
			it++) {
		if (typeid(**it).name() == typeid(RelationLeafOperator).name()) {
			boost::shared_ptr<RelationLeafOperator> op =
					boost::static_pointer_cast<RelationLeafOperator>(*it);
			op->execution();
			operatorList.erase(it++);
			break;
		}

	}
	it = this->operatorList.begin();
}

bool Scheduler::isEmpty(void) {
	boost::mutex::scoped_lock lk(registeOperatorListMutex);
	return this->operatorList.size() == 0;
}

void Scheduler::setOperatorList(
		std::list<boost::shared_ptr<Operator> > operatorList) {
	//std::cout << "setting op list in scheduler " << std::endl;
	boost::mutex::scoped_lock lk(registeOperatorListMutex);

	this->operatorList = operatorList;
	this->it = this->operatorList.begin();

}


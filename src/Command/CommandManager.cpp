#include "../Common/stdafx.h"
#include "../Command/CommandManager.h"
#include "../IO/IOManager.h"
#include "../Common/Types.h"
#include "../BinaryJson/json.h"
#include "../IO/RandomGeneratedStreamInput.h"
#include "../Server/JsonStreamServer.h"
#include "../IO/SocketStreamInput.h"
#include "../Query/QueryIntermediateRepresentation.h"
#include "../IO/SocketStreamOutput.h"
#include "../Parser/ParserManager.h"
#include "../IO/IStreamOutput.h"
#include "../IO/IStreamInput.h"
#include "../IO/ShowResultStreamOutput.h"
#include "../Query/QueryManager.h"
#include "../Plan/PlanManager.h"
#include "../Scheduler/Scheduler.h"
#include "../Wrapper/WrapperManager.h"
#include "../Server/JsonStreamServer.h"
CommandManager* CommandManager::commandManager = NULL;
CommandManager::CommandManager(void)
{
	initial();
}


CommandManager::~CommandManager(void)
{
}
CommandManager* CommandManager::getInstance()
{
	if(commandManager==NULL)
	{
		commandManager = new CommandManager();
	}
	return commandManager;
}
//use IOManager to listen to a port waiting for command
void CommandManager::initial()
{
	IOManager::getInstance()->addCommandInput(SERVER_IP,SERVER_PORT,this);
}
//the call back function called by IO manager, whenever new commands receive, this function would be called
//some kinds of command:
//1.register wrapper
//2.register query
bool CommandManager::processCommand(char* command,size_t length)
{
	
	Document commandDocument(command);
	
	std::string commandType = commandDocument.getField(COMMAND_TYPE).valuestr();
	if(commandType == REGISTER_WRAPPER)
	{
		processRegisterWrapperCommand(commandDocument);
	}
	else if(commandType == REGISTER_QUERY)
	{
		processRegisterQueryCommand(commandDocument);
	}
	else
	{
		assert(false); // never reached
		return false;
	}

	
	return true;
	 
}
bool CommandManager::processRegisterWrapperCommand(Document& document)
{
	Document wrapperDocument = document.getField(WRAPPER_CONTENT).embeddedObject();
	WrapperManager::getInstance()->registerWrapper(wrapperDocument);
	return true;
}
bool CommandManager::processRegisterQueryCommand(Document& document)
{
	std::string jaqlQueryString = document.getField(QUERY_CONTENT).valuestr();
	std::cout<<"Jqal query accepted : " << jaqlQueryString<<std::endl;
	boost::shared_ptr<QueryIntermediateRepresentation> intermediateQuery = ParserManager::getInstance()->processQuery(jaqlQueryString);
	Document outputSpecificationDocument = document.getField(OUTPUT_SPECIFICATION).embeddedObject();
	std::string outputType =  outputSpecificationDocument.getField(OUTPUT_TYPE).valuestr();
	std::string queryDuration = outputSpecificationDocument.getField(QUERY_DURATION).valuestr();
	if(outputType == SHOW_RESULT)
	{
		boost::shared_ptr<IStreamOutput> streamOutput(new ShowResultStreamOutput());
		JsonStreamServer::getInstance()->registerQuery(intermediateQuery, streamOutput,queryDuration);		
	}
	else if(outputType == SOCKET_OUTPUT)
	{
		std::string ip = outputSpecificationDocument.getField(OUTPUT_IP).valuestr();
		std::string port = outputSpecificationDocument.getField(OUTPUT_PORT).valuestr();
		boost::shared_ptr<IStreamOutput> streamOutput(new SocketStreamOutput(ip,port));
		
		JsonStreamServer::getInstance()->registerQuery(intermediateQuery, streamOutput,queryDuration);	
	}
	else 
	{
		assert(false);//never reached
		return false;
	}
	return true;
}

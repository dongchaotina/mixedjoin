#pragma once
#include "../Common/stdafx.h"
#include "../Query/QueryIntermediateRepresentation.h"
#include <boost/shared_ptr.hpp>

class ParserManager
{
private:
	static  ParserManager * parserManager;
	ParserManager(void);
	
	static const int parserServerPort = 3000;
	void init();
	void connectServer();
public:
	static ParserManager * getInstance();
	~ParserManager(void);
	boost::shared_ptr<QueryIntermediateRepresentation>processQuery(std::string jaqlQueryString);
};


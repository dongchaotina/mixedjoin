#pragma once
#include "../Common/stdafx.h"

#ifdef WINDOWS
#define NOINLINE_DECL __declspec(noinline)
#else
#define NOINLINE_DECL __attribute__((noinline))
#endif
#define PACKED_DECL

// branch prediction.  indicate we expect to be false
# define MONGO_unlikely(x) ((bool)(x))

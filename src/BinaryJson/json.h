

#pragma once

#include "../Common/stdafx.h"
#include <string>
#include "../BinaryJson/BinaryJsonObject.h"

/* build binary JSON object by JSON character string*/
BinaryJsonObject fromjson(const std::string &str);

/** len will be size of JSON object in text chars. */
BinaryJsonObject fromjson(const char *str, int* len=NULL);

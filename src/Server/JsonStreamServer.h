#pragma once
#include "../Common/stdafx.h"
#include "../Scheduler/Scheduler.h"
#include "../Operator/Operator.h"
#include "../IO/IStreamInput.h"
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include "../IO/IStreamInput.h"
#include "../IO/IStreamOutput.h"
#include "../Schema/JsonSchema.h"
#include "../Query/QueryIntermediateRepresentation.h"
#include "../Command/CommandManager.h"
#include "../IO/IOManager.h"
#include "../Common/Types.h"
#include "../BinaryJson/json.h"
#include "../IO/RandomGeneratedStreamInput.h"
#include "../Server/JsonStreamServer.h"
#include "../IO/SocketStreamInput.h"
#include "../IO/SocketStreamOutput.h"
#include "../IO/IStreamOutput.h"
#include "../IO/IStreamInput.h"
#include "../IO/ShowResultStreamOutput.h"
#include "../Query/QueryManager.h"
#include "../Plan/PlanManager.h"
#include "../Scheduler/Scheduler.h"
#include "../Wrapper/WrapperManager.h"
#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
/* the JSON server engine 
 */

class JsonStreamServer:private boost::noncopyable
{
private:
	
	static JsonStreamServer* jsonStreamServer;
	JsonStreamServer(void);
	~JsonStreamServer(void);
	void initial();



public:

	static JsonStreamServer* getInstance(void);
	void execute();

	void registerQuery(std::string jaqlQueryString, boost::shared_ptr<IStreamOutput> streamOutput, std::string durationSpecification);
	void registerQuery(boost::shared_ptr<QueryIntermediateRepresentation>queryIntermediateRepresentation, boost::shared_ptr<IStreamOutput> streamOutput, std::string durationSpecification);
	void registerStreamInput(boost::shared_ptr<IStreamInput> streamInput);
	
};


#include "../Common/stdafx.h"
#include "../Server/JsonStreamServer.h"
#include "../BinaryJson/json.h"
#include "../Internal/Memory/MemoryManager.h"
#include "../Common/Types.h"
#include "../Internal/Element/Element.h"
#include "../Utility/TimestampGenerator.h"
#include "../Utility/DocumentIdentifierGenerator.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Internal/Synopsis/RelationSynopsis.h"
#include "../Internal/Synopsis/LineageSynopsis.h"
#include "../Server/JsonStreamServer.h"
#include "../Schema/JsonSchema.h"
#include "../Schema/SchemaInterpreter.h"
#include "../Operator/Operator.h"
#include "../Operator/RowWindowOperator.h"
#include "../Operator/JoinOperator.h"
#include "../Operator/SelectionOperator.h"
#include "../Operator/ProjectionOperator.h"
#include "../Operator/RootOperator.h"
#include "../Operator/RstreamOperator.h"
#include "../Operator/IstreamOperator.h"
#include "../Operator/DstreamOperator.h"
#include "../Operator/LeafOperator.h"
#include "../Scheduler/Scheduler.h"
#include "../Operator/SmartRowWindowOperator.h"
#include "../Utility/TimeCounterUtility.h"
#include "../IO/IOManager.h"
#include "../IO/SocketStreamInput.h"
#include "../IO/SpecifiedInputRateStreamInput.h"
#include "../IO/IStreamInput.h"
#include "../IO/RandomGeneratedStreamInput.h"
#include "../IO/ShowResultStreamOutput.h"
#include "../IO/TokyoPeopleFlowStreamInputShort.h"
#include "../IO/IStreamOutput.h"
#include "../Query/QueryManager.h"
#include "../Query/QueryIntermediateRepresentation.h"
#include "../Schema/JsonSchema.h"
#include "../Plan/PlanManager.h"
#include "../Parser/ParserManager.h"
#include "../Schema/SchemaManager.h"
#include "../Command/CommandManager.h"
#include "../Configure/ConfigureManager.h"
#include "../Wrapper/WrapperManager.h"
#include "../Query/QueryEntity.h"
#include <sys/time.h>
#include <unistd.h>
#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

#include "../IO/DBManager.h"


JsonStreamServer* JsonStreamServer::jsonStreamServer = NULL;
JsonStreamServer::JsonStreamServer(void) {
	initial(); //initial the server 
}

JsonStreamServer::~JsonStreamServer(void) {

}
JsonStreamServer* JsonStreamServer::getInstance(void) {

	if (jsonStreamServer == NULL) {
		jsonStreamServer = new JsonStreamServer();
	}
	return jsonStreamServer;
}
/* initial each module manager in the system*/
void JsonStreamServer::initial() {

	std::cout << "initial" << std::endl;
	ConfigureManager::getInstance(); // read configuration file, this manager should be initialed first
	std::cout << "configuration initialed" << std::endl;
	IOManager::getInstance();        // deal with IO socket 
	std::cout << "IOManager initialed" << std::endl;
	MemoryManager::getInstance(); // allocate and manage memory for queues and synopsis
	std::cout << "MemoryManager initialed" << std::endl;
	PlanManager::getInstance(); // generate query plan and instantiate operators by query intermediate representation
	std::cout << "PlanManager initialed" << std::endl;
	ParserManager::getInstance(); // manage the input and output of the query registered
	std::cout << "ParserManager initialed" << std::endl;
	QueryManager::getInstance(); // manage the input and output of the query registered
	std::cout << "QueryManager initialed" << std::endl;
	SchemaManager::getInstance();    // register schema information
	std::cout << "SchemaManager initialed" << std::endl;
	CommandManager::getInstance(); // system listens to a port waiting for command
	std::cout << "CommandManager initialed" << std::endl;
	WrapperManager::getInstance(); // read the wrapper specification in the wrapper folder, and register each wrapper
	std::cout << "WrapperManager initialed" << std::endl;

	/*DBManager::getInstance(); // read the wrapper specification in the wrapper folder, and register each wrapper
	std::cout << "DBManager initialed" << std::endl;*/

}

/* server execution, each time get one operator and the execution times from the scheduler */
void JsonStreamServer::execute() {
	struct timeval begin, end;
	IOManager::getInstance()->execute();
	TimeCounterUtility::getInstance()->reset();

	Scheduler::getInstance()->checkRelationLeafOperator();

	gettimeofday(&begin, NULL);
	while (1) {

		boost::shared_ptr<Operator> op;

		if (!Scheduler::getInstance()->isEmpty()) // check if there is an operator waiting to execute
		{ //yes
		  //	std::cout<<"---"<<QueueEntity::total_dequeue_number<<std::endl;
			gettimeofday(&end, NULL);
			double diff = (end.tv_sec - begin.tv_sec)
					+ (end.tv_usec - begin.tv_usec) / 1000000;
			int systemExecutionSecond = 30;
			if (diff > systemExecutionSecond) //system running 60 second
					{
				double totalTime;
				TimeCounterUtility::getInstance()->stop(totalTime);

				std::cout << "the execution finished, total time : "
						<< systemExecutionSecond << std::endl;
				boost::shared_ptr<IStreamInput> specifiedStreamInput1 =
						QueryManager::getInstance()->getRegisteredStreamById(
								"tokyoPeopleFlowStreamShort");
				boost::shared_ptr<TokyoPeopleFlowStreamInputShort> stream1 =
						boost::dynamic_pointer_cast<
						TokyoPeopleFlowStreamInputShort>(
								specifiedStreamInput1);
				std::cout << "master stream1 : processing  number: "
						<< (int) stream1->tokyoPeopleTuplesCounter << std::endl;

				boost::shared_ptr<RelationInput> relationInput =
						QueryManager::getInstance()->QueryManager::getRegisteredRelationById(
								"relation1");
			/*	std::cout << "relationInput : processing number : "
						<< relationInput->relationTuplesNumber << std::endl;

				std::cout << "total number : "
						<< stream1->totalElementNumber
								- (int) stream1->bufferedElementNumber
								+ relationInput->relationTuplesNumber
						<< std::endl;*/

				break;
			}
			Scheduler::getInstance()->getNextOperatorToExecute(op);
			op->execution(); //execute the operator

		} else { //release CPU resource
				 //Sleep(5000);

		}
	}

}

void JsonStreamServer::registerQuery(std::string jaqlQueryString,
		boost::shared_ptr<IStreamOutput> streamOutput,
		std::string durationSpecification) {
	boost::shared_ptr<QueryIntermediateRepresentation> intermediateQuery =
			ParserManager::getInstance()->processQuery(jaqlQueryString);
	registerQuery(intermediateQuery, streamOutput, durationSpecification);
}
void JsonStreamServer::registerQuery(
		boost::shared_ptr<QueryIntermediateRepresentation> queryIntermediateRepresentation,
		boost::shared_ptr<IStreamOutput> streamOutput,
		std::string durationSpecification) {

	boost::shared_ptr<QueryEntity> queryEntity =
			QueryManager::getInstance()->addQuery(
					queryIntermediateRepresentation, streamOutput,
					durationSpecification);
	PlanManager::getInstance()->createQueryPlan(queryEntity); // generate plan
	Scheduler::getInstance()->setOperatorList(
			PlanManager::getInstance()->getAllOperators()); // inform scheduler

}
void JsonStreamServer::registerStreamInput(
		boost::shared_ptr<IStreamInput> streamInput) {
	QueryManager::getInstance()->registerStream(streamInput); //register query
}

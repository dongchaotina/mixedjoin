#include "../Common/stdafx.h"
#include "../Configure/ConfigureManager.h"
#include <fstream>
#include <boost/algorithm/string.hpp>
#include<boost/unordered_map.hpp>
ConfigureManager * ConfigureManager::configure = NULL;
ConfigureManager::ConfigureManager(void)
{
	initial();
}


ConfigureManager::~ConfigureManager(void)
{
}
ConfigureManager* ConfigureManager::getInstance(void)
{
	if(configure==NULL)
	{
		configure = new ConfigureManager();
	}
	return configure;
}
void ConfigureManager::initial()
{
	//std::cout<<"reading configure files"<<std::endl;
	std::string configureFilePath = CONFIGURE_FILE_PATH;
	std::ifstream fin(configureFilePath.c_str());
	std::string s;
	while(getline(fin,s))
	{
		//std::cout<<s<<std::endl;
		std::string key = s.substr(0,s.find('='));
		boost::trim(key);
		std::string value = s.substr(s.find('=')+1,s.length());
		boost::trim(value);
		//std::cout<<key<<std::endl;
		//std::cout<<value<<std::endl;
		this->configuration.insert(make_pair(key,value));
	}
}


std::string ConfigureManager::getConfigureValue(std::string key)
{
	boost::associative_property_map< boost::unordered_map<std::string, std::string> >
		configuration(this->configuration);
	return configuration[key];

}
boost::unordered_map<std::string, std::string> ConfigureManager::getAllConfigureValue(void)
{
	return this->configuration;
}

#include "../Common/stdafx.h"
#include "../Common/Types.h"
JSONTYPE generateJsonType(std::string type)
{
	JSONTYPE jsonType;
	if(type=="string")
	{
		jsonType = JSON_STRING;
	}
	else if (type == "number")
	{
		jsonType = JSON_NUMBER;
	}
	else if (type == "object")
	{
		jsonType = JSON_OBJECT;
	}
	else if (type == "array")
	{
		jsonType = JSON_ARRAY;
	}
	else if (type == "true")
	{
		jsonType = JSON_TRUE;
	}
	else if (type == "false")
	{
		jsonType = JSON_FALSE;
	}
	else if (type == "null")
	{
		jsonType = JSON_NULL;
	}
	else 
	{
		throw runtime_error("no such json types");
	}
	return jsonType;
}
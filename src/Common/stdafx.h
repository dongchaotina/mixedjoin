#pragma once


#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
#include <stdio.h>
#include <assert.h>
#include <list>
#include <map>
#include <algorithm>
#include <strstream>
#include <iostream>
#include <fstream>
#include <queue>
//#define DEBUG  // the system will run in debug mode
//#define LOG    // the output will be write into a file
//#define SMART //the system will turns into smart mode, or will turns into naive mode.
//#define PROFILER // the system will runs in a performance test mode, after the execution, the profiler file would be generated.
//#define CHECKINPUTSCHEMA // the system will checks the schema of each input tuples

#ifdef LOG
static std::ofstream logFile("server1_out.txt");
static std::streambuf *outbuf = std::cout.rdbuf(logFile.rdbuf());
#endif

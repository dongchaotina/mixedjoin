#pragma once
#include "../../Common/stdafx.h"
#include <boost/noncopyable.hpp>
#include <boost/thread.hpp>
/*
   allocate a large size of continuous memory when initials
   divide it into pages
   manage the allocation and release of the pages 
 */
class MemoryManager:private boost::noncopyable
{
	
	MemoryManager(void);
	static MemoryManager* memoryManager;
	char* buffer;
	void initial(void );
	boost::mutex m_oMutex;
	 
	std::list<int>freePageList;
	char* getPageByIndex(int index);

public:
	static MemoryManager* getInstance(void);
	~MemoryManager(void);
	char* getOnePage(void);
	void releaseOnePage(char*);
	static int getPageSize(void);

};


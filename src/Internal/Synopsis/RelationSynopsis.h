#pragma once
#include "../../Common/stdafx.h"
#include "../../Internal/Element/Element.h"
#include <boost/shared_ptr.hpp>
#include "../../Internal/Buffer/RelationBuffer.h"
#include "../../Internal/Buffer/BufferConstraint.h"
#include "../../Internal/Synopsis/Synopsis.h"
class ElementIterator;
class SetElementIterator;
class RelationSynopsis: public Synopsis 
{
private:

	boost::shared_ptr<RelationBuffer>buffer;
public:
	RelationSynopsis(void);
	virtual ~RelationSynopsis(void);
	void insertElement(Element& element);
	bool deleteElement(Element& element);
	void clear(void);
	bool isFull(void);
	friend class ElementIterator;
	friend class SetElementIterator;
};

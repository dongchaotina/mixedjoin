/*
 * RelationSynopsisForROp.h
 *
 *  Created on: Jun 2, 2015
 *      Author: root
 */

#ifndef SRC_INTERNAL_SYNOPSIS_RELATIONSYNOPSISFORROP_H_
#define SRC_INTERNAL_SYNOPSIS_RELATIONSYNOPSISFORROP_H_

#include "RelationSynopsis.h"

class RelationSynopsisForROp: public RelationSynopsis {
public:
	RelationSynopsisForROp();
	virtual ~RelationSynopsisForROp();

	void querySynopsisByAttribute(std::string attrA, int newValueA,
			std::string condiA, int condiValueA);

};

#endif /* SRC_INTERNAL_SYNOPSIS_RELATIONSYNOPSISFORROP_H_ */

#pragma once
#include "../../Common/stdafx.h"
#include "../../Common/Types.h"
#include "../../Internal/Buffer/LineageBuffer.h"
#include "../../Internal/Buffer/BufferConstraint.h"
#include "../../Internal/Element/Element.h"
#include <boost/shared_ptr.hpp>
#include "../../Internal/Synopsis/Synopsis.h"
class LineageSynopsis:public Synopsis
{
private:
	boost::shared_ptr<LineageBuffer>buffer;
	const int lineageNumber;
public:
	LineageSynopsis(int lineageNumber);
	virtual ~LineageSynopsis(void);
	
	void insertLineage(Lineage& lineage,Element& outputElement);
	bool getAndDeleteElement(Lineage& lineage, Element& outputElement);
	bool isFull(void);
};


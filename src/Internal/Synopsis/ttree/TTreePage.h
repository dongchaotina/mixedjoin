/*
 * TTreePage.h
 *
 *  Created on: Oct 7, 2015
 *      Author: root
 */

#ifndef SRC_INTERNAL_SYNOPSIS_TTREE_TTREEPAGE_H_
#define SRC_INTERNAL_SYNOPSIS_TTREE_TTREEPAGE_H_

#include <boost/shared_ptr.hpp>
#include "../../../Common/stdafx.h"
#include "../../../Common/Types.h"

#include "../../../Internal/Buffer/BufferConstraint.h"
#include "../../../Internal/Element/Element.h"
#include "../../../Internal/Buffer/AbstractBuffer.h"
#include "../../../Internal/Memory/MemoryManager.h"

using namespace boost;
using namespace std;

enum {
	TNODE_UNDEF = -1, /**< T*-tree node side is undefined */
	TNODE_LEFT, /**< Left side */
	TNODE_RIGHT,
/**< Right side */
};

static int __balance_factors[] = { -1, 1 };

/**
 * Default number of keys per T*-tree node
 */

#define KEYS_PER_NODE CHUNK_NUMBER_ONE_PAGE

/**
 * Minimum allowed number of keys per T*-tree node
 */
//#define TNODE_ITEMS_MIN 2
/**
 * Maximum allowed numebr of keys per T*-tree node
 */

#define TNODE_ROOT  TNODE_UNDEF /**< T*-tree node is root */
#define TNODE_BOUND TNODE_UNDEF /**< T*-tree node bounds searhing value */

namespace ttree {

//node

class TTreePage {

public:

	char* buffer;

	TTreePage();
	virtual ~TTreePage();

	TTreePage* parent;
	TTreePage* successor;

	union {
		TTreePage* sides[2];
		struct {
			TTreePage* left; /**< Pointer to node's left child  */
			TTreePage* right; /**< Pointer to node's right child */
		};
	};

	union {
		uint32_t pad;
		struct {
			CHUNK_INDEX_TYPE min_idx :12; /**< Index of minimum item in node's array */
			CHUNK_INDEX_TYPE max_idx :12; /**< Index of maximum item in node's array */
			signed bfc :4; /**< Node's balance factor */
			unsigned node_side :4; /**< Node's side(TNODE_LEFT, TNODE_RIGHT or TNODE_ROOT) */
		};
	};

	/**
	 * First two items of T*-tree node keys array
	 */
	//void *keys[TNODE_ITEMS_MIN];
	static __inline signed side2bfc(int side) {
		return __balance_factors[side];
	}

	signed get_bfc_delta() {
		return side2bfc(this->tnode_get_side());
	}

	__inline void tnode_set_side(int side) {
		node_side &= ~0x3;
		node_side |= (side + 1);
	}

	__inline int tnode_get_side() {

		int ret = (node_side & 0x03) - 1;

		return ret;
	}

	__inline signed tnode_num_keys() {
		return max_idx - min_idx + 1;
	}

	__inline bool tnode_is_empty() {
		return !tnode_num_keys();
	}

	__inline bool tnode_is_full() {
		return tnode_num_keys() == KEYS_PER_NODE;
	}

	/*__inline int tnode_size() {
	 return sizeof(TTreePage) + (KEYS_PER_NODE - TNODE_ITEMS_MIN)
	 * sizeof(uintptr_t);
	 }*/

	inline void setValue(CHUNK_INDEX_TYPE chunkIndex, Element& element) {
		assert(element.getSize() <= CHUNK_SIZE);
		assert(chunkIndex <= CHUNK_NUMBER_ONE_PAGE);

		char* chunkPostion = this->buffer + getHeaderSize()
				+ chunkIndex * CHUNK_SIZE;
		int p = sizeof(CHUNK_INDEX_TYPE) + sizeof(PAGE_INDEX_TYPE);

		//todo a little waste

		*(Timestamp*) (chunkPostion + p) = element.timestamp;
		//memcpy(chunkPostion + p , &element.timestamp,TIMESTAMP_SIZE);
		p += TIMESTAMP_SIZE;
		//memcpy(chunkPostion + p, &element.id, DOCUMENT_IDENTIFIER_SIZE);
		*(DocumentId*) (chunkPostion + p) = element.id;
		p += DOCUMENT_IDENTIFIER_SIZE;
		//memcpy(chunkPostion + p, &element.mark, MARK_SIZE);
		*(Mark*) (chunkPostion + p) = element.mark;
		p += MARK_SIZE;

		*(MasterTag*) (chunkPostion + p) = element.masterTag;
		p += MASTER_TAG_SIZE;

		memcpy(chunkPostion + p, element.document.objdata(),
				element.document.objsize());

	}
	inline void getValue(CHUNK_INDEX_TYPE chunkIndex, Element& element) {
		assert(chunkIndex <= CHUNK_NUMBER_ONE_PAGE);
		char* chunkPostion = this->buffer + +getHeaderSize()
				+ chunkIndex * CHUNK_SIZE;
		int p = sizeof(CHUNK_INDEX_TYPE) + sizeof(PAGE_INDEX_TYPE);

		element.timestamp = *(Timestamp*) (chunkPostion + p);
		p += TIMESTAMP_SIZE;
		element.id = *(DocumentId*) (chunkPostion + p);
		p += DOCUMENT_IDENTIFIER_SIZE;
		element.mark = *(Mark*) (chunkPostion + p);
		p += MARK_SIZE;
		element.masterTag = *(MasterTag*) (chunkPostion + p);
		p += MASTER_TAG_SIZE;
		element.document = Document(chunkPostion + p);

	}

	inline int getFieldByIdx(CHUNK_INDEX_TYPE idx, string field) {
		Element element;
		getValue(idx, element);
		Document document = element.document;
		return document.getField(field).Int();
	}

	__inline bool subtree_is_unbalanced() {
		return (bfc < -1) || (bfc > 1);
	}

	__inline bool left_heavy() {
		return bfc < 0;
	}
	__inline bool right_heavy() {
		return bfc > 0;
	}

	__inline bool is_internal_node() {
		return left && right;
	}

	__inline bool is_half_leaf() {
		return ((!left || !right) && !(left && right));
	}

	__inline bool is_leaf_node() {
		return (!left && !right);
	}

private:
	std::size_t getHeaderSize() {
		/*return sizeof(this->parent) + sizeof(this->successor)
		 + sizeof(this->sides) + sizeof(this->pad);*/
		return 0;
	}
};

} /* namespace ttree */

#endif /* SRC_INTERNAL_SYNOPSIS_TTREE_TTREEPAGE_H_ */

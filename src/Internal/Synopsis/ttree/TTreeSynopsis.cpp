/*
 * TTreeSynopsis.cpp
 *
 *  Created on: Oct 7, 2015
 *      Author: root
 */

#include "TTreeSynopsis.h"

namespace ttree {

TTreeSynopsis::TTreeSynopsis() {
	// TODO Auto-generated constructor stub
	this->buffer.reset(new TTreeBuffer());
	boost::shared_ptr<TTreeBuffer> secondbuffer;
	secondbuffer.reset(new TTreeBuffer());
	this->secondBuffers.push_back(secondbuffer);

}

TTreeSynopsis::~TTreeSynopsis() {
	// TODO Auto-generated destructor stub
}

void TTreeSynopsis::insertElement(Element& element) {

	TTreeCursor cursur = this->buffer->insertElement(element);

	shared_ptr<TTreeBuffer> secondBuffer = this->secondBuffers.at(0);
	int indexValue = secondBuffer->getIndexValueByElement(element);

	DocumentBuilder newDocumentBuilder;
	newDocumentBuilder.append(secondBuffer->indexName, indexValue);
	newDocumentBuilder.append(buffer->indexName,
			element.document.getField("PK").Int());

	Element newElement;
	newElement.mark = 't';
	newElement.id = 0;
	newElement.timestamp = 0;
	newElement.masterTag = false;
	newElement.document = newDocumentBuilder.obj();

	secondBuffer->insertElement(newElement);

}
bool TTreeSynopsis::deleteElement(Element& element) {
	this->buffer->deleteElement(element);
	this->secondBuffers.at(0)->deleteElement(element);
	return true;
}

bool TTreeSynopsis::isFull(void) {
	bool bl = this->buffer->isFull();
	return bl;
}

bool TTreeSynopsis::findElementListByKeyInBuffer(int key,
		std::vector<Element>& elements) {

	return this->buffer->findElementListByKey(key, elements);
}

bool TTreeSynopsis::findElementListByKeyIn2ndBuffer(int key,
		std::vector<Element>& elements) {

	std::vector<Element> newElements;

	bool found = this->secondBuffers.at(0)->findElementListByKey(key,
			newElements);
	vector<Element>::iterator it;
	for (it = newElements.begin(); it != newElements.end(); it++) {

		int pk = it->document.getField("PK").Int();

		std::vector<Element> newPKElements;
		this->findElementListByKeyInBuffer(pk, newPKElements);
		vector<Element>::iterator itt;
		for (itt = newPKElements.begin(); itt != newPKElements.end(); itt++) {
			elements.push_back(*itt);
		}

	}

	return found;
}

} /* namespace ttree */

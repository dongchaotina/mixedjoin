/*
 * TTreeBuffer.cpp
 *
 *  Created on: Oct 7, 2015
 *      Author: root
 */

#include "TTreeBuffer.h"
#include <assert.h>
namespace ttree {

static int __cmpfunc(int key1, int key2) {
	return key1 - key2;
}

static __inline unsigned opposite_side(unsigned side) {
	return !side;
}
static int __balance_factors[] = { -1, 1 };

static __inline signed side2bfc(unsigned side) {
	return __balance_factors[side];
}

TTreeBuffer::TTreeBuffer() {
	// TODO Auto-generated constructor stub
	this->root = NULL;
	//this->freePositionHead.pageIndex = 0;
	//this->freePositionHead.chunkIndex = 0;

	TTreePage* elementPage(new TTreePage);
	this->pageVector.push_back(elementPage);
	this->pageNumber = 1;

	keys_per_tnode = CHUNK_NUMBER_ONE_PAGE;
	cmp_func = __cmpfunc;
	keys_are_unique = false;

	num = 0;
}

TTreeBuffer::~TTreeBuffer() {
	// TODO Auto-generated destructor stub
}

bool TTreeBuffer::findElementListByKey(int key,
		std::vector<Element>& elements) {

	Element element;

	TTreeCursor cursor;
	bool found = findElementByKey(key, &cursor, element);

	if (found) {

		elements.push_back(element);

	} else {

		return false;
	}

	TTreePage *n = cursor.tnode;
	int i = cursor.idx + 1;

	int tempKey = n->getFieldByIdx(i, indexName);

	while (tempKey == key) {

		Element newElement;
		n->getValue(i, newElement);
		elements.push_back(newElement);
		i++;

		if (n->successor == NULL && i > n->max_idx) {
			break;
		}

		if (i > n->max_idx && n->successor != NULL) {

			n = n->successor;
			i = n->min_idx;
		}

		tempKey = n->getFieldByIdx(i, indexName);

	}

	return true;
}

bool TTreeBuffer::findElementByKey(int key, TTreeCursor *cursor,
		Element& element) {

	TTreePage *n, *marked_tn, *target;
	int side = TNODE_BOUND, cmp_res;
	CHUNK_INDEX_TYPE idx;

	bool found = false;

	enum ttree_cursor_state st = CURSOR_PENDING;

	/*
	 * Classical T-tree search algorithm is O(log(2N/M) + log(M - 2))
	 * Where N is total number of items in the tree and M is a number of
	 * items per node. In worst case each node on the path requires 2
	 * comparison(with its min and max items) plus binary search in the last
	 * node(bound node) excluding its first and last items.
	 *
	 * Here is used another approach that was suggested in
	 * "Tobin J. Lehman , Michael J. Carey, A Study of Index Structures for
	 * Main Memory Database Management Systems".
	 * It reduces O(log(2N/M) + log(M - 2)) to true O(log(N)).
	 * This algorithm compares the search
	 * key only with minimum item in each node. If search key is greater,
	 * current node is marked for future consideration.
	 */
	target = n = root;
	marked_tn = NULL;
	idx = first_tnode_idx();

	if (n == NULL) {
		goto out;
	}
	while (n != NULL) {
		target = n;

		cmp_res = this->cmp_func(key, n->getFieldByIdx(n->min_idx, indexName));
		if (cmp_res < 0)
			side = TNODE_LEFT;
		else if (cmp_res > 0) {
			marked_tn = n; /* mark current node for future consideration. */
			side = TNODE_RIGHT;
		} else { /* ok, key is found, search is completed. */
			side = TNODE_BOUND;
			idx = n->min_idx;
			n->getValue(idx, element);
			found = true;
			st = CURSOR_OPENED;
			goto out;
		}

		n = n->sides[side];
	}
	if (marked_tn != NULL) {

		int c = cmp_func(key,
				marked_tn->getFieldByIdx(marked_tn->max_idx, indexName));

		if (c <= 0) {
			side = TNODE_BOUND;
			target = marked_tn;
			if (!c) {
				idx = target->max_idx;
				target->getValue(idx, element);
				found = true;
				st = CURSOR_OPENED;
			} else { /* make internal binary search */
				struct tnode_lookup tnl;

				tnl.key = key;
				tnl.low_bound = target->min_idx + 1;
				tnl.high_bound = target->max_idx - 1;

				found = lookup_inside_tnode(target, &tnl, &idx, element);

				st = found ? CURSOR_OPENED : CURSOR_PENDING;
			}

			goto out;
		}
	}

	/*
	 * If we're here, item wasn't found. So the only thing
	 * needs to be done is to determine the position where search key
	 * may be placed to. If target node is not empty, key may be placed
	 * to its min or max positions.
	 */
	if (!target->tnode_is_full()) {
		side = TNODE_BOUND;
		idx = ((marked_tn != target) || (cmp_res < 0)) ?
				target->min_idx : (target->max_idx + 1);
		st = CURSOR_PENDING;
	}
	out: if (cursor) {
		ttree_cursor_open_on_node(cursor, target, TNODE_SEEK_START);
		cursor->side = side;
		cursor->idx = idx;
		cursor->state = st;
	}
	return found;

}

int TTreeBuffer::ttree_cursor_open_on_node(TTreeCursor *cursor,
		TTreePage *tnode, enum tnode_seek seek) {

	memset(cursor, 0, sizeof(*cursor));
	cursor->tnode = tnode;

	/*
	 * If T*-tree node was specified, the cursor becomes
	 * ready for iteration. Otherwise we suppose that T*-tree
	 * is completely empty, so it becomes ready for insertion.
	 * In second case seek argument is ignored.
	 */
	if (tnode) {
		switch (seek) {
		case TNODE_SEEK_START:
			cursor->idx = tnode->min_idx;
			break;
		case TNODE_SEEK_END:
			cursor->idx = tnode->max_idx;
			break;
		default:
			return -1;
		}

		cursor->state = CURSOR_OPENED;
	} else {
		//cursor->idx = first_tnode_idx(cursor->ttree);
		cursor->idx = 0;
		cursor->state = CURSOR_PENDING;
	}

	cursor->side = TNODE_BOUND;
	return 0;
}

bool TTreeBuffer::lookup_inside_tnode(TTreePage *tnode,
		struct tnode_lookup *tnl, CHUNK_INDEX_TYPE *out_idx, Element& element) {
	int floor, ceil, mid, cmp_res;

	floor = tnl->low_bound;
	ceil = tnl->high_bound;

	while (floor <= ceil) {
		mid = (floor + ceil) >> 1;
		if ((cmp_res = cmp_func(tnl->key, tnode->getFieldByIdx(mid, indexName)))
				< 0)
			ceil = mid - 1;
		else if (cmp_res > 0)
			floor = mid + 1;
		else {
			*out_idx = mid;
			tnode->getValue(mid, element);
			return true;
		}
	}

	/*
	 * If a key position is not found, save an index of the position
	 * where key may be placed to.
	 */
	*out_idx = floor;
	return false;
}

TTreeCursor TTreeBuffer::insertElement(Element& element) {
	TTreeCursor cursor;
	num++;

	Element newElement;

	//cout << "insert " << element.document.getField(this->indexName) << endl;
	if (findElementByKey(getIndexValueByElement(element), &cursor, newElement)
			&& keys_are_unique) {
		//return NULL;
	}

	ttree_insert_at_cursor(&cursor, element);

	//__print_tree(root, 0);
	//cout <<__node_count(root) << endl;

	return cursor;
}

void element_copy(Element src, Element &dest) {

	dest.document = src.document.copy();
	dest.id = src.id;
	dest.mark = src.mark;
	dest.masterTag = src.masterTag;
	dest.timestamp = src.timestamp;

}

void TTreeBuffer::ttree_insert_at_cursor(TTreeCursor *cursor, Element item) {
	TTreePage *at_node, *n;
	TTreeCursor tmp_cursor;
	Element key;

	//key = item;

	element_copy(item, key);

	n = at_node = cursor->tnode;
	if (!root) { /* The root node has to be created. */

		/*if (!allocate_new_page()) {
		 return;
		 }*/

		at_node = pageVector.at(this->pageNumber - 1);

		//at_node->keys[first_tnode_idx()] = key;
		at_node->setValue(first_tnode_idx(), item);
		at_node->min_idx = at_node->max_idx = first_tnode_idx();

		root = at_node;
		at_node->tnode_set_side(TNODE_ROOT);
		ttree_cursor_open_on_node(cursor, at_node, TNODE_SEEK_START);
		return;
	}
	if (cursor->side == TNODE_BOUND) {
		if (n->tnode_is_full()) {
			/*
			 * If node is full its max item should be removed and
			 * new key should be inserted into it. Removed key becomes
			 * new insert value that should be put in successor node.
			 */

			Element tmp;
			n->getValue(n->max_idx--, tmp);

			Document doc = tmp.document.copy();
			DocumentId id = tmp.id;
			Mark mark = tmp.mark;
			MasterTag masterTag = tmp.masterTag;
			Timestamp timestamp = tmp.timestamp;

			increase_tnode_window(n, &cursor->idx);
			n->setValue(cursor->idx, key);

			key.document = doc;
			key.id = id;
			key.mark = mark;
			key.masterTag = masterTag;
			key.timestamp = timestamp;

			ttree_cursor_copy(&tmp_cursor, cursor);
			cursor = &tmp_cursor;

			/*
			 * If current node hasn't successor and right child
			 * New node have to be created. It'll become the right child
			 * of the current node.
			 */
			if (!n->successor || !n->right) {
				cursor->side = TNODE_RIGHT;
				cursor->idx = first_tnode_idx();
				goto create_new_node;
			}

			at_node = n->successor;
			/*
			 * If successor hasn't any free rooms, new value is inserted
			 * into newly created node that becomes left child of the current
			 * node's successor.
			 */
			if (at_node->tnode_is_full()) {
				cursor->side = TNODE_LEFT;
				cursor->idx = first_tnode_idx();
				goto create_new_node;
			}

			/*
			 * If we're here, then successor has free rooms and key
			 * will be inserted to one of them.
			 */
			cursor->idx = at_node->min_idx;
			cursor->tnode = at_node;
		}

		increase_tnode_window(at_node, &cursor->idx);
		at_node->setValue(cursor->idx, key);
		cursor->state = CURSOR_OPENED;
		return;
	}

	create_new_node: allocate_new_page();

	n = this->pageVector.at(this->pageNumber - 1);

	n->setValue(cursor->idx, key);

	n->min_idx = n->max_idx = cursor->idx;
	n->parent = at_node;
	at_node->sides[cursor->side] = n;
	n->tnode_set_side(cursor->side);
	cursor->tnode = n;
	cursor->state = CURSOR_OPENED;
	fixup_after_insertion(n, cursor);
}

static __inline void __add_successor(TTreePage *n) {
	/*
	 * After new leaf node was added, its successor should be
	 * fixed. Also it(successor) could became a successor of the node
	 * higher than the given one.
	 * There are several possible cases of such situation:
	 * 1) If new node is added as a right child, it inherites
	 *    successor of its parent. And it itself becomes a successor
	 *    of its parent.
	 * 2) If it is a left child, its parent will be the successor.
	 * 2.1) If parent itself is a right child, then newly added node becomes
	 *      the successor of parent's parent.
	 * 2.2) Otherwise it becomes a successor of one of nodes located higher.
	 *      In this case, we should browse up the tree starting from
	 *      parent's parent. One of the nodes on the path *may* have a successor
	 *      equals to parent of a newly added node. If such node will be found,
	 *      its successor should be changed to a newly added node.
	 */
	if (n->tnode_get_side() == TNODE_RIGHT) {
		n->successor = n->parent->successor;
		n->parent->successor = n;
	} else {
		n->successor = n->parent;
		if (n->parent->tnode_get_side() == TNODE_RIGHT) {
			n->parent->parent->successor = n;
		} else if (n->parent->tnode_get_side() == TNODE_LEFT) {
			register TTreePage *node;

			for (node = n->parent->parent; node; node = node->parent) {
				if (node->successor == n->parent) {
					node->successor = n;
					break;
				}
			}
		}
	}
}

void TTreeBuffer::fixup_after_insertion(TTreePage *n, TTreeCursor *cursor) {
	int bfc_delta = n->get_bfc_delta();
	TTreePage *node = n;

	__add_successor(n);
	/* check tree for balance after new node was added. */
	while ((node = node->parent)) {
		node->bfc += bfc_delta;
		/*
		 * if node becomes balanced, tree balance is ok,
		 * so process may be stopped here
		 */
		if (!node->bfc) {
			return;
		}
		if (node->subtree_is_unbalanced()) {
			/*
			 * Because of nature of T-tree rebalancing, just inserted item
			 * may change its position in its node and even the node itself.
			 * Thus if T-tree cursor was specified we have to take care of it.
			 */
			rebalance(&node, cursor);

			/*
			 * single or double rotation tree becomes balanced
			 * and we can stop here.
			 */
			return;
		}

		bfc_delta = node->get_bfc_delta();
	}
}

static void __rotate_single(TTreePage **target, int side) {
	TTreePage *p, *s;
	int opside = opposite_side(side);

	p = *target;
	s = p->sides[side];
	s->tnode_set_side(p->tnode_get_side());
	p->sides[side] = s->sides[opside];
	s->sides[opside] = p;
	p->tnode_set_side(opside);
	s->parent = p->parent;
	p->parent = s;
	if (p->sides[side]) {
		p->sides[side]->parent = p;
		p->sides[side]->tnode_set_side(side);
	}
	if (s->parent) {
		if (s->parent->sides[side] == p)
			s->parent->sides[side] = s;
		else
			s->parent->sides[opside] = s;
	}

	*target = s;
}

static void rotate_single(TTreePage **target, int side) {
	TTreePage *n;

	__rotate_single(target, side);
	n = (*target)->sides[opposite_side(side)];

	/*
	 * Recalculate balance factors of nodes after rotation.
	 * Let X was a root node of rotated subtree and Y was its
	 * child. After single rotation Y is new root of subtree and X is its child.
	 * Y node may become either balanced or overweighted to the
	 * same side it was but 1 level less.
	 * X node scales at 1 level down and possibly it has new child, so
	 * its balance should be recalculated too. If it still internal node and
	 * its new parent was not overwaighted to the opposite to X side,
	 * X is overweighted to the opposite to its new parent side, otherwise it's balanced.
	 * If X is either half-leaf or leaf, balance racalculation is obvious.
	 */
	if (n->is_internal_node()) {
		n->bfc = (n->parent->bfc != side2bfc(side)) ? side2bfc(side) : 0;
	} else {
		n->bfc = !!(n->right) - !!(n->left);
	}

	(*target)->bfc += side2bfc(opposite_side(side));
}

/*
 * There are two possible cases of double rotation:
 * 1) Left-right rotation: (side == TNODE_LEFT)
 *      [P]                     [r]
 *     /  \                    /  \
 *   [L]  x1                [L]   [P]
 *  /  \          =>       / \    / \
 * x2  [r]                x2 x4  x3 x1
 *    /  \
 *  x4   x3
 *
 * 2) Right-left rotation: (side == TNODE_RIGHT)
 *      [P]                     [l]
 *     /  \                    /  \
 *    x1  [R]               [P]   [R]
 *       /  \     =>        / \   / \
 *      [l] x2             x1 x3 x4 x2
 *     /  \
 *    x3  x4
 */
static void rotate_double(TTreePage **target, int side) {
	int opside = opposite_side(side);
	TTreePage *n = (*target)->sides[side];

	__rotate_single(&n, opside);

	/*
	 * Balance recalculation is very similar to recalculation after
	 * simple single rotation.
	 */
	if (n->sides[side]->is_internal_node()) {
		n->sides[side]->bfc = (n->bfc == side2bfc(opside)) ? side2bfc(side) : 0;
	} else {
		n->sides[side]->bfc = !!(n->sides[side]->right)
				- !!(n->sides[side]->left);
	}

	n = n->parent;
	__rotate_single(target, side);
	if (n->is_internal_node()) {
		n->bfc = ((*target)->bfc == side2bfc(side)) ? side2bfc(opside) : 0;
	} else {
		n->bfc = !!(n->right) - !!(n->left);
	}

	/*
	 * new root node of subtree is always ideally balanced
	 * after double rotation.
	 */
	(*target)->bfc = 0;
}

void TTreeBuffer::rebalance(TTreePage **node, TTreeCursor *cursor) {
	int lh = (*node)->left_heavy();
	int sum = abs((*node)->bfc + (*node)->sides[opposite_side(lh)]->bfc);

	if (sum >= 2) {
		rotate_single(node, opposite_side(lh));
		goto out;
	}

	rotate_double(node, opposite_side(lh));

	/*
	 * T-tree rotation rules difference from AVL rules in only one aspect.
	 * After double rotation is done and a leaf became a new root node of
	 * subtree and both its left and right childs are half-leafs.
	 * If the new root node contains only one item, N - 1 items should
	 * be moved into it from one of its childs.
	 * (N is a number of items in selected child node).
	 */
	if (((*node)->tnode_num_keys() == 1) && (*node)->left->is_half_leaf()
			&& (*node)->right->is_half_leaf()) {
		TTreePage *n;
		int offs, nkeys;

		/*
		 * If right child contains more items than left, they will be moved
		 * from the right child. Otherwise from the left one.
		 */
		if ((*node)->right->tnode_num_keys()
				>= (*node)->left->tnode_num_keys()) {
			/*
			 * Right child was selected. So first N - 1 items will be copied
			 * and inserted after parent's first item.
			 */
			n = (*node)->right;
			nkeys = n->tnode_num_keys();

			//(*node)->keys[0] = (*node)->keys[(*node)->min_idx];
			setValue((*node), (*node)->min_idx, (*node), 0);

			offs = 1;
			(*node)->min_idx = 0;
			(*node)->max_idx = nkeys - 1;
			if (!cursor) {
				goto no_cursor;
			} else if (cursor->tnode == n) {
				if (cursor->idx < n->max_idx) {
					cursor->tnode = *node;
					cursor->idx = (*node)->min_idx
							+ (cursor->idx - n->min_idx + 1);
				} else {
					cursor->idx = first_tnode_idx();
				}
			}
		} else {
			/*
			 * Left child was selected. So its N - 1 items
			 * (starting after the min one)
			 * will be copied and inserted before parent's single item.
			 */
			n = (*node)->left;
			nkeys = n->tnode_num_keys();

			//(*node)->keys[ttree->keys_per_tnode - 1] =
			//		(*node)->keys[(*node)->min_idx];

			setValue((*node), (*node)->min_idx, (*node), keys_per_tnode - 1);

			(*node)->min_idx = offs = keys_per_tnode - nkeys;
			(*node)->max_idx = keys_per_tnode - 1;
			if (!cursor) {
				goto no_cursor;
			} else if (cursor->tnode == n) {
				if (cursor->idx > n->min_idx) {
					cursor->tnode = *node;
					cursor->idx = (*node)->min_idx + (cursor->idx - n->min_idx);
				} else {
					cursor->idx = first_tnode_idx();
				}
			}

			n->max_idx = n->min_idx++;
		}

		//no_cursor: memcpy((*node)->keys + offs, n->keys + n->min_idx,
		//		sizeof(void *) * (nkeys - 1));

		no_cursor:

		for (int i = 0; i < nkeys - 1; i++) {

			setValue(n, i + n->min_idx, (*node), i + offs);

		}
		//chunk_memcpy((*node), offs, n, n->min_idx, nkeys);
		//n->keys[first_tnode_idx()] = n->keys[n->max_idx];

		setValue(n, n->max_idx, n, first_tnode_idx());

		n->min_idx = n->max_idx = first_tnode_idx();
	}

	out: if (root->parent) {
		root = *node;
	}
}

TTreeCursor TTreeBuffer::deleteElement(Element& element) {
	TTreeCursor cursor;
	bool ret;
	ret = findElementByKey(getIndexValueByElement(element), &cursor, element);
	//cout << "delete: " << element.document.getField(this->indexName) << endl;
	if (!ret) {
		//return NULL;
	}
	ttree_delete_at_cursor(&cursor);
	//__print_tree(root, 0);
	//cout << indexName << ": " << __node_count(root) << endl;
	return cursor;
}

bool TTreeBuffer::ttree_delete_at_cursor(TTreeCursor *cursor) {
	TTreePage *tnode, *n;

	tnode = cursor->tnode;

	//ret = ttree_key2item(ttree, tnode->keys[cursor->idx]);
	//tnode->getValue(cursor->idx, ret);

	decrease_tnode_window(tnode, &cursor->idx);
	cursor->state = CURSOR_CLOSED;
	if (cursor->idx > tnode->max_idx) {
		cursor->idx = tnode->max_idx;
	}

	/*
	 * If after a key was removed, T*-tree node contains more than
	 * minimum allowed number of items, the proccess is completed.
	 */
	if (tnode->tnode_num_keys() > min_tnode_entries()) {
		return true;
	}

	/*if(root->tnode_num_keys() <= 4){
	 cout << "here" << endl;
	 }*/

	if (tnode->is_internal_node()) {
		int idx;

		/*
		 * If it is an internal node, we have to recover number
		 * of items from it by moving one item from its successor.
		 */
		n = tnode->successor;
		idx = tnode->max_idx + 1;
		increase_tnode_window(tnode, &idx);

		//tnode->keys[idx] = n->keys[n->min_idx++];
		setValue(n, n->min_idx++, tnode, idx);

		if (cursor->idx > tnode->max_idx) {
			cursor->idx = tnode->max_idx;
		}
		if (!n->tnode_is_empty() && n->is_leaf_node()) {
			return true;
		}

		/*
		 * If we're here, then successor is either a half-leaf
		 * or an empty leaf.
		 */
		tnode = n;
	}
	if (!tnode->is_leaf_node()) {
		int items, diff;

		n = tnode->left ? tnode->left : tnode->right;
		items = n->tnode_num_keys();

		/*
		 * If half-leaf can not be merged with a leaf,
		 * the proccess is completed.
		 */
		if (items > (keys_per_tnode - tnode->tnode_num_keys())) {
			return true;
		}

		if (n->tnode_get_side() == TNODE_RIGHT) {
			/*
			 * Merge current node with its right leaf. Items from the leaf
			 * are placed after the maximum item in a node.
			 */
			diff = (keys_per_tnode - tnode->max_idx - items) - 1;
			if (diff < 0) {

				/*memcpy(tnode->keys + tnode->min_idx + diff,
				 tnode->keys + tnode->min_idx,
				 sizeof(void *) * tnode_num_keys(tnode));*/

				/*chunk_memcpy(tnode, tnode->min_idx + diff, tnode,
				 tnode->min_idx, tnode->tnode_num_keys());*/

				for (int i = 0; i < tnode->tnode_num_keys(); i++) {
					setValue(tnode, i + tnode->min_idx, tnode,
							i + diff + tnode->min_idx);
				}

				tnode->min_idx += diff;
				tnode->max_idx += diff;
				if (cursor->tnode == tnode) {
					cursor->idx += diff;
				}
			}

			/*memcpy(tnode->keys + tnode->max_idx + 1, n->keys + n->min_idx,
			 sizeof(void *) * items);
			 */
			//chunk_memcpy(tnode, tnode->max_idx + 1, n, n->min_idx, items);
			for (int i = 0; i < items; i++) {
				setValue(n, i + n->min_idx, tnode, i + tnode->max_idx + 1);
			}

			tnode->max_idx += items;
		} else {
			/*
			 * Merge current node with its left leaf. Items the leaf
			 * are placed before the minimum item in a node.
			 */
			diff = tnode->min_idx - items;
			if (diff < 0) {
				register int i;

				for (i = tnode->max_idx; i >= tnode->min_idx; i--) {
					//tnode->keys[i - diff] = tnode->keys[i];
					setValue(tnode, i, tnode, i - diff);
				}

				tnode->min_idx -= diff;
				tnode->max_idx -= diff;
				if (cursor->tnode == tnode) {
					cursor->idx -= diff;
				}
			}

			/*memcpy(tnode->keys + tnode->min_idx - items, n->keys + n->min_idx,
			 sizeof(void *) * items);*/

			//chunk_memcpy(tnode, tnode->min_idx - items, n, n->min_idx, items);
			for (int i = 0; i < items; i++) {
				setValue(n, i + n->min_idx, tnode, i + tnode->min_idx - items);
			}

			tnode->min_idx -= items;
		}

		n->min_idx = 1;
		n->max_idx = 0;
		tnode = n;
	}
	if (!tnode->tnode_is_empty()) {
		return true;
	}

	/* if we're here, then current node will be removed from the tree. */
	n = tnode->parent;
	if (n == NULL) {
		root = NULL;
		free(tnode);
		return true;
	}

	n->sides[tnode->tnode_get_side()] = NULL;

	fixup_after_deletion(tnode, NULL);

	free(tnode);
	return true;
}

__inline void __remove_successor(TTreePage *n) {
	/*
	 * Node removing could affect the successor of one of nodes
	 * with higher level, so it should be fixed.
	 * Since T*-tree node deletion algorithm
	 * assumes that ony leafs are removed, successor fixing
	 * is opposite to successor adding algorithm.
	 */
	if (n->tnode_get_side() == TNODE_RIGHT) {
		n->parent->successor = n->successor;
	} else if (n->parent->tnode_get_side() == TNODE_RIGHT) {
		n->parent->parent->successor = n->parent;
	} else {
		register TTreePage *node = n;

		while ((node = node->parent)) {
			if (node->successor == n) {
				node->successor = n->parent;
				break;
			}
		}
	}
}

void TTreeBuffer::fixup_after_deletion(TTreePage *n, TTreeCursor *cursor) {
	TTreePage *node = n->parent;
	int bfc_delta = n->get_bfc_delta();

	__remove_successor(n);

	/*
	 * Unlike balance fixing after insertion,
	 * deletion may require several rotations.
	 */
	while (node) {
		node->bfc -= bfc_delta;
		/*
		 * If node's balance factor was 0 and becomes 1 or -1, we can stop.
		 */
		if (!(node->bfc + bfc_delta))
			break;

		bfc_delta = node->get_bfc_delta();
		if (node->subtree_is_unbalanced()) {
			TTreePage *tmp = node;

			rebalance(&tmp, cursor);
			/*
			 * If after rotation subtree height is not changed,
			 * proccess should be continued.
			 */
			if (tmp->bfc)
				break;

			node = tmp;
		}

		node = node->parent;
	}
}

void TTreeBuffer::__print_tree(TTreePage *tnode, int offs) {
	int i;

	for (i = 0; i < offs; i++)
		cout << " ";
	if (!tnode) {
		cout << "(nil)\n";
		return;
	}

	if (tnode->tnode_get_side() == TNODE_LEFT)
		cout << "[L] ";
	else if (tnode->tnode_get_side() == TNODE_RIGHT)
		cout << "[R] ";
	else
		cout << "[*] ";

	for (i = 0; i < offs + 1; i++)
		cout << " ";

	cout << "<" << tnode->tnode_num_keys() << ">";
	/* if (fn) {
	 fn(tnode);
	 }*/
	/*cout << "%d", tnode->getKeyValueByIdx(tnode->min_idx));
	 cout << " ... ");
	 cout << "%d", tnode->getKeyValueByIdx(tnode->max_idx));
	 cout << "\n");*/

	for (int i = tnode->min_idx; i < tnode->max_idx + 1; i++) {
		cout << tnode->getFieldByIdx(i, indexName) << " ";
	}
	cout << endl;

	//__print_tree(tnode->left, offs + 1);
	//__print_tree(tnode->right, offs + 1);
}

int TTreeBuffer::__node_count(TTreePage *tnode) {

	if (!tnode) {
		return 0;
	}

	int count = 0;
	count += tnode->tnode_num_keys();

	count += __node_count(tnode->left);
	count += __node_count(tnode->right);

	return count;
}

} /* namespace ttree */

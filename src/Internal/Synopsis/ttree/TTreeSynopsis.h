/*
 * TTreeSynopsis.h
 *
 *  Created on: Oct 7, 2015
 *      Author: root
 */

#ifndef SRC_INTERNAL_SYNOPSIS_TTREE_TTREESYNOPSIS_H_
#define SRC_INTERNAL_SYNOPSIS_TTREE_TTREESYNOPSIS_H_

#include "TTreeBuffer.h"
#include "../../../Internal/Synopsis/Synopsis.h"

namespace ttree {

class TTreeSynopsis: public Synopsis {

private:

public:
	TTreeSynopsis();
	virtual ~TTreeSynopsis();

	boost::shared_ptr<TTreeBuffer> buffer;
	std::vector<boost::shared_ptr<TTreeBuffer> > secondBuffers;

	void insertElement(Element& element);
	bool deleteElement(Element& element);
	bool findElementListByKeyInBuffer(int key, std::vector<Element>& element);
	bool findElementListByKeyIn2ndBuffer(int key, std::vector<Element>& element);
	void clear(void);
	bool isFull(void);

};
}
/* namespace ttree */

#endif /* SRC_INTERNAL_SYNOPSIS_TTREE_TTREESYNOPSIS_H_ */

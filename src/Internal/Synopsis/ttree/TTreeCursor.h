/*
 * TTreeCursor.h
 *
 *  Created on: Oct 18, 2015
 *      Author: root
 */

#ifndef SRC_INTERNAL_SYNOPSIS_TTREE_TTREECURSOR_H_
#define SRC_INTERNAL_SYNOPSIS_TTREE_TTREECURSOR_H_

#include "TTreePage.h"

namespace ttree {

enum ttree_cursor_state {
	CURSOR_CLOSED = 0, CURSOR_OPENED, CURSOR_PENDING,
};

class TTreeCursor {
public:
	TTreeCursor();
	virtual ~TTreeCursor();
	TTreePage* tnode;
	int idx; /**< Particular index in a T*-tree node array */
	int side;
	enum ttree_cursor_state state;
};

} /* namespace ttree */

#endif /* SRC_INTERNAL_SYNOPSIS_TTREE_TTREECURSOR_H_ */

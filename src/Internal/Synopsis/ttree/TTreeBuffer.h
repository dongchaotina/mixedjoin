/*
 * TTreeBuffer.h
 *
 *  Created on: Oct 7, 2015
 *      Author: root
 */

#ifndef SRC_INTERNAL_SYNOPSIS_TTREE_TTREEBUFFER_H_
#define SRC_INTERNAL_SYNOPSIS_TTREE_TTREEBUFFER_H_

#include "TTreePage.h"
#include "TTreeCursor.h"
#include <list>
#include <boost/unordered_map.hpp>

typedef int (*ttree_cmp_func_fn)(int key1, int key2);

enum tnode_seek {
	TNODE_SEEK_START, TNODE_SEEK_END,
};

namespace ttree {

//tree

class TTreeBuffer {

private:

	int pageNumber; // how many pages are allocated for this buffer
	std::vector<TTreePage*> pageVector; // the vector contains the pages

	struct tnode_lookup {
		int key;
		int low_bound;
		int high_bound;
	};

	int ttree_cursor_open_on_node(TTreeCursor* cursor, TTreePage * tnode,
			enum tnode_seek seek);
	void ttree_insert_at_cursor(TTreeCursor* cursor, Element item);

	bool ttree_delete_at_cursor(TTreeCursor *cursor);
	void fixup_after_insertion(TTreePage *n, TTreeCursor *cursor);
	void fixup_after_deletion(TTreePage *n, TTreeCursor *cursor);
	void rebalance(TTreePage **node, TTreeCursor *cursor);
	void __print_tree(TTreePage *tnode, int offs);
	int __node_count(TTreePage *tnode);
	void chunk_memcpy(TTreePage* node, int offs, TTreePage* n, int offs2,
			int nkeys);

	void setValue(TTreePage* src, CHUNK_INDEX_TYPE idx1, TTreePage* dest,
			CHUNK_INDEX_TYPE idx2) {

		Element element;
		src->getValue(idx1, element);
		dest->setValue(idx2, element);

	}
	__inline void increase_tnode_window(TTreePage *tnode, int *idx) {
		register int i;

		/*
		 * If the right side of an array has more free rooms than the left one,
		 * the window will grow to the right. Otherwise it'll grow to the left.
		 */

		if ((keys_per_tnode - 1 - tnode->max_idx) > tnode->min_idx) {
			for (i = ++tnode->max_idx; i > *idx - 1; i--) {
				setValue(tnode, i - 1, tnode, i);
			}
		} else {
			*idx -= 1;
			for (i = --tnode->min_idx; i < *idx; i++) {
				setValue(tnode, i + 1, tnode, i);
			}
		}
	}

	__inline void decrease_tnode_window(TTreePage *tnode, int *idx) {
		register int i;

		/* Shrink the window to the longer side by given index. */

		if ((keys_per_tnode - 1 - tnode->max_idx) <= tnode->min_idx) {
			tnode->max_idx--;
			for (i = *idx; i <= tnode->max_idx; i++)
				setValue(tnode, i + 1, tnode, i);
		} else {
			tnode->min_idx++;
			for (i = *idx; i >= tnode->min_idx; i--)
				setValue(tnode, i - 1, tnode, i);

			*idx = *idx + 1;
		}
	}

public:

	TTreeBuffer();
	virtual ~TTreeBuffer();

	int num;

	string indexName;

	TTreePage *root; /**< A pointer to T*-tree root node */
	ttree_cmp_func_fn cmp_func; /**< User-defined key comparing function */
	int keys_per_tnode; /**< Number of keys per each T*-tree node */

	/**
	 * The field is true if keys in a tree supposed to be unique
	 */
	bool keys_are_unique;

	bool findElementByKey(int key, TTreeCursor *cursor, Element& element);
	bool findElementListByKey(int key, std::vector<Element>& elements);

	__inline bool ttree_is_empty() {
		return !root;
	}

	__inline int min_tnode_entries() {
		return keys_per_tnode - (keys_per_tnode >> 2);
	}

	void *ttree_lookup(void *key, TTreeCursor *cursor);
	bool lookup_inside_tnode(TTreePage *tnode, struct tnode_lookup *tnl,
			CHUNK_INDEX_TYPE *out_idx, Element& element);

	inline bool allocate_new_page(void) //allocate a new page from memory manager
			{
		if (this->pageNumber == MAX_PAGE_NUMBER_IN_ONE_QUEUE_OR_SYNOPSIS) {
			return false;
		}
		TTreePage* elementPage(new TTreePage);
		this->pageVector.push_back(elementPage);
		this->pageNumber++;

		/*if (pageNumber % 10 == 0) {
		 __print_tree(root, 0);
		 }*/

		return true;
	}

	inline int first_tnode_idx() {
		return ((keys_per_tnode >> 1) - 1);
	}

	TTreeCursor insertElement(Element& element);
	TTreeCursor deleteElement(Element& element);

	int getIndexValueByElement(Element element) {
		return element.document.getField(indexName).Int();
	}

	inline bool isValidMemoryPosition(MemoryPosition& memoryPosition) // if a memory position is valid in this buffer
			{

		return memoryPosition.pageIndex >= 0
				&& memoryPosition.pageIndex <= this->pageNumber - 1
				&& memoryPosition.chunkIndex >= 0
				&& memoryPosition.chunkIndex <= CHUNK_NUMBER_ONE_PAGE - 1;
	}

	inline bool isFull(void) {
		bool ret;
		//todo
		return false;
	}

	__inline void ttree_cursor_copy(TTreeCursor *csr_dst,
			TTreeCursor *csr_src) {
		memcpy(csr_dst, csr_src, sizeof(*(csr_src)));
	}

};

} /* namespace ttree */

#endif /* SRC_INTERNAL_SYNOPSIS_TTREE_TTREEBUFFER_H_ */

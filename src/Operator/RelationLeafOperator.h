/*
 * RelationOperator.h
 *
 *  Created on: May 21, 2015
 *      Author: root
 */

#ifndef SRC_OPERATOR_RELATIONLEAFOPERATOR_H_
#define SRC_OPERATOR_RELATIONLEAFOPERATOR_H_

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/any.hpp>
#include <event2/event.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>

#include "../Internal/Synopsis/ttree/TTreeSynopsis.h"
#include "../Internal/Synopsis/RelationSynopsis.h"
#include "../Internal/Synopsis/LineageSynopsis.h"
#include "../Internal/Element/ElementIterator.h"
#include "../IO/RelationInput.h"
#include "TTreeJoinOperator.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Query/QueryExpressionAbstract.h"
#include "../Query/QueryProjectionAbstract.h"
#include "../Utility/SleepUtility.h"




class RelationLeafOperator: public Operator {

private:


	void updateSynopsis(std::vector<std::string> vecSegTag);
	void insertSynopsis(std::vector<std::string> vecSegTag);
	void deleteSynopsis(std::vector<std::string> vecSegTag);


public:

	boost::shared_ptr<RelationInput> relationInput;

	boost::recursive_mutex m_oMutex;

	RelationLeafOperator();
	virtual ~RelationLeafOperator();
	void execution();
	void byGenerate();
	void outputNewcomeElements(Element oldElement, Element newElement);
	void parseAndExecuteUpdate(Document dbdataDoc);
	boost::shared_ptr<ttree::TTreeSynopsis> getJoinSynopsis();
	const boost::shared_ptr<RelationInput>& getRelationInput() const {
		return relationInput;
	}

	void setRelationInput(
			const boost::shared_ptr<RelationInput>& relationInput) {
		this->relationInput = relationInput;
	}
};

#endif /* SRC_OPERATOR_RELATIONLEAFOPERATOR_H_ */

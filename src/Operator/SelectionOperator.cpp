#include "../Common/stdafx.h"
#include "../Operator/SelectionOperator.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Query/QueryConditionAbstract.h"
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include "../Scheduler/Scheduler.h"
#include "../Utility/TimeCounterUtility.h"
SelectionOperator::SelectionOperator(void)
{
	
}


SelectionOperator::~SelectionOperator(void)
{
}


void SelectionOperator::execution()
{
	
#ifdef DEBUG
	std::cout<<"===================operator begin================="<<std::endl;
	std::cout<<"operatorid : "<<this->getId()<<std::endl;
#endif
	assert(this->getInputQueueList().size()==1);
	assert(this->getOutputQueueList().size()==1);
	boost::shared_ptr<QueueEntity>inputQueue = this->getInputQueueList().front();
	boost::shared_ptr<QueueEntity>outputQueue = this->getOutputQueueList().front();
	while(1)
	{
		Element inputElement;
		if(outputQueue->isFull())
		{
			break;
		}
		if(inputQueue->isEmpty())
		{
			break;
		}
		TimeCounterUtility::getInstance()->start();
		inputQueue->dequeue(inputElement);
		if(isSatisfySelectionCondition(inputElement))
		{

			outputQueue->enqueue(inputElement);
		}
		TimeCounterUtility::getInstance()->pause();

	}
#ifdef DEBUG
	std::cout<<"===================operator over================="<<std::endl;
#endif
}

void SelectionOperator::setSelectionCondition(boost::shared_ptr<QueryConditionAbstract> condition)
{
	this->condition = condition;
}

bool SelectionOperator::isSatisfySelectionCondition(Element& element)
{
	return this->condition->getValue(element);
	
}

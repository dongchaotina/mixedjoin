/*
 * TTreeJoinOperator.h
 *
 *  Created on: Oct 19, 2015
 *      Author: root
 */

#pragma once
#include "../Common/stdafx.h"
#include "../Operator/Operator.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Internal/Synopsis/ttree/TTreeSynopsis.h"
#include "../Internal/Synopsis/RelationSynopsis.h"
#include "../Internal/Synopsis/LineageSynopsis.h"
#include "../Operator/Operator.h"
#include "../Internal/Element/ElementIterator.h"
#include <boost/shared_ptr.hpp>
#include "../Query/QueryExpressionAbstract.h"
#include "../Query/QueryProjectionAbstract.h"

#ifndef SRC_OPERATOR_TTREEJOINOPERATOR_H_
#define SRC_OPERATOR_TTREEJOINOPERATOR_H_

namespace ttree {

class TTreeJoinOperator: public Operator {

private:
	boost::shared_ptr<LineageSynopsis> lineageSynopsis;
	boost::shared_ptr<RelationSynopsis> leftRelationSynopsis;
	boost::shared_ptr<TTreeSynopsis> rightTTreeSynopsis;

	boost::shared_ptr<QueryAttribute> leftJoinAttribute;
	boost::shared_ptr<QueryAttribute> rightJoinAttribute;
	bool leftOuter;
	bool rightOuter;

	boost::shared_ptr<ElementIterator> leftElementIterator;

	boost::shared_ptr<QueryProjectionAbstract> resultQueryProjection;


public:
	TTreeJoinOperator();
	virtual ~TTreeJoinOperator();

	void initialTTreeSynopsis();

	void execution();
	void processLeftPlus(Element& leftElement);
	void processLeftMinus(Element& leftElement);
	void processRightPlus(Element& rightElement);
	void processRightMinus(Element& rightElement);

	void processLeftPlusDirect(Element& rightElement);
	void processLeftMinusDirect(Element& rightElement);

	void processReset();
	void setLeftJoinAttribute(
			boost::shared_ptr<QueryAttribute> leftJoinAttribute);
	void setRightJoinAttribute(
			boost::shared_ptr<QueryAttribute> rightJoinAttribute);
	void setLeftOuter(bool bl);
	void setRightOuter(bool bl);
	void setResultQueryProjection(
			boost::shared_ptr<QueryProjectionAbstract> resultQueryProjection);

	const boost::shared_ptr<TTreeSynopsis>& getRightTTreeSynopsis() const {
		return rightTTreeSynopsis;
	}
};

} /* namespace ttree */

#endif /* SRC_OPERATOR_TTREEJOINOPERATOR_H_ */

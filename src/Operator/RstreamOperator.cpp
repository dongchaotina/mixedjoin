#include "../Common/stdafx.h"
#include "../Operator/RstreamOperator.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Utility/TimeCounterUtility.h"
#include "../Utility/DocumentIdentifierGenerator.h"
#include "../Scheduler/Scheduler.h"
RstreamOperator::RstreamOperator(void) {
	this->relationSynopsis.reset(new RelationSynopsis());
	this->lastInputTimestamp = 0;
	this->elementIterator.reset(new ElementIterator(this->relationSynopsis));
}

RstreamOperator::~RstreamOperator(void) {
}

void RstreamOperator::execution() {
#ifdef DEBUG
	std::cout<<"===================operator begin================="<<std::endl;
	std::cout<<"operatorid : "<<this->getId()<<std::endl;
#endif
	assert(this->getInputQueueList().size() == 1);
	assert(this->getOutputQueueList().size() == 1);
	boost::shared_ptr<QueueEntity> inputQueue =
			this->getInputQueueList().front();
	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();

	Element inputElement;
	while (1) {
		if (outputQueue->isFull()) {
			break;
		}
		if (inputQueue->isEmpty()) {
			break;
		}
		TimeCounterUtility::getInstance()->start();
		inputQueue->dequeue(inputElement);
		/*	cout << "document:" << inputElement.document << endl;

		 for(;lastInputTimestamp<inputElement.timestamp;lastInputTimestamp++)
		 {
		 cout << "document:" << inputElement.document << endl;
		 produceOutput();
		 assert(!outputQueue->isFull());
		 }
		 */

		//cout << inputElement << endl;
		if (inputElement.mark == PLUS_MARK) {
			this->relationSynopsis->insertElement(inputElement);
		} else if (inputElement.mark == MINUS_MARK) {
			this->relationSynopsis->deleteElement(inputElement);
		} else if (inputElement.mark == '0') {
			this->relationSynopsis.reset(new RelationSynopsis());
		}

		TimeCounterUtility::getInstance()->pause();
	}

#ifdef DEBUG
	std::cout<<"===================operator over================="<<std::endl;
#endif
}
void RstreamOperator::produceOutput() {

	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();

	this->elementIterator->initial();
	Element elementInSynopsis;
	while ((!outputQueue->isFull())
			&& this->elementIterator->getNext(elementInSynopsis)) {
		Element outputElement;
		outputElement.document = elementInSynopsis.document;
		outputElement.id = elementInSynopsis.id;
		outputElement.mark = PLUS_MARK;
		outputElement.timestamp = lastInputTimestamp;
		outputQueue->enqueue(outputElement);
	}
	assert(!outputQueue->isFull());
}

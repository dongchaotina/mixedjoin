#pragma once
#include "../Common/stdafx.h"
#include "../Operator/Operator.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Internal/Synopsis/LineageSynopsis.h"
#include "../Internal/Synopsis/RelationSynopsis.h"
#include "../Operator/Operator.h"
#include "../Internal/Element/ElementIterator.h"
#include <boost/shared_ptr.hpp>
class IstreamOperator: public Operator
{
private:

	boost::shared_ptr<RelationSynopsis>relationSynopsis;
	boost::shared_ptr<ElementIterator> elementIterator;
	Timestamp lastInputTimestamp;
	void produceOutput();
public:
	IstreamOperator(void);
	~IstreamOperator(void);
	void execution();
	
};


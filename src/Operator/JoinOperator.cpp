#include "../Common/stdafx.h"
#include "../Operator/JoinOperator.h"
#include "../Internal/Synopsis/LineageSynopsis.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Utility/DocumentIdentifierGenerator.h"
#include "../Internal/Synopsis/RelationSynopsis.h"
#include "../Internal/Element/ElementIterator.h"
#include "../Scheduler/Scheduler.h"
#include "../Utility/TimeCounterUtility.h"
#include "../Query/QueryUtility.h"

#include "../Utility/DocumentIdentifierGenerator.h"
#include "../Utility/TimestampGenerator.h"
#include "../Utility/TimeCounterUtility.h"
#include "../BinaryJson/json.h"

#include "../IO/DBManager.h"
using namespace oracle::occi;

#include <occi.h>

JoinOperator::JoinOperator(void) {
	int lineageNumber = 2;
	this->lineageSynopsis.reset(new LineageSynopsis(lineageNumber));
	this->leftRelationSynopsis.reset(new RelationSynopsis());
	this->rightRelationSynopsis.reset(new RelationSynopsis());
//	this->lastLeftTimestamp = 0;
//	this->lastRightTimestamp = 0;
	this->leftElementIterator.reset(
			new ElementIterator(this->leftRelationSynopsis));
	this->rightElementIterator.reset(
			new ElementIterator(this->rightRelationSynopsis));
	this->leftOuter = false;
	this->rightOuter = false;

}

JoinOperator::~JoinOperator(void) {
}

void JoinOperator::setLeftJoinAttribute(
		boost::shared_ptr<QueryAttribute> leftJoinAttribute) {
	this->leftJoinAttribute = leftJoinAttribute;
}
void JoinOperator::setRightJoinAttribute(
		boost::shared_ptr<QueryAttribute> rightJoinAttribute) {
	this->rightJoinAttribute = rightJoinAttribute;
}
void JoinOperator::setLeftOuter(bool bl) {
	this->leftOuter = bl;
}
void JoinOperator::setRightOuter(bool bl) {
	this->rightOuter = bl;
}
void JoinOperator::setResultQueryProjection(
		boost::shared_ptr<QueryProjectionAbstract> resultQueryProjection) {
	this->resultQueryProjection = resultQueryProjection;
}

void JoinOperator::execution() {
#ifdef DEBUG
	std::cout<<"===================operator begin================="<<std::endl;
	std::cout<<"operatorid : "<<this->getId()<<std::endl;
#endif
	assert(this->getInputQueueList().size() == 2);
	assert(this->getOutputQueueList().size() == 1);
	boost::shared_ptr<QueueEntity> leftInputQueue =
			this->getInputQueueList().front();
	boost::shared_ptr<QueueEntity> rightInputQueue =
			this->getInputQueueList().back();
	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();

	long long minimumLeftQueueLastTimestamp = -1;
	long long minimumRightQueueLastTimestamp = -1;
	//minimumLeftQueueLastTimestamp = lastLeftTimestamp;
	//minimumRightQueueLastTimestamp = lastRightTimestamp;

	while (1) {
		Element leftElement;
		Element rightElement;
		if (outputQueue->isFull()) {
			break;
		}
		if (!leftInputQueue->isEmpty()) {
			leftInputQueue->peek(leftElement);
			minimumLeftQueueLastTimestamp = leftElement.timestamp;
		}
		if (!rightInputQueue->isEmpty()) {
			rightInputQueue->peek(rightElement);
			minimumRightQueueLastTimestamp = rightElement.timestamp;
		}
		if (leftElement.timestamp == -1 && rightElement.timestamp == -1) {
			break;
		}

		if ((!leftInputQueue->isEmpty()) && (!rightInputQueue->isEmpty())
				&& minimumLeftQueueLastTimestamp
						< minimumRightQueueLastTimestamp) {
			assert(!leftInputQueue->isEmpty());
			TimeCounterUtility::getInstance()->start();
			leftInputQueue->dequeue();
			//lastLeftTimestamp = leftElement.timestamp;
			if (leftElement.mark == PLUS_MARK) {
				processLeftPlus(leftElement);
			} else if (leftElement.mark == MINUS_MARK) {
				processLeftMinus(leftElement);
			}
			TimeCounterUtility::getInstance()->pause();
		} else if ((!leftInputQueue->isEmpty()) && (!rightInputQueue->isEmpty())
				&& minimumLeftQueueLastTimestamp
						> minimumRightQueueLastTimestamp) {
			assert(!rightInputQueue->isEmpty());
			TimeCounterUtility::getInstance()->start();
			rightInputQueue->dequeue();
			//lastRightTimestamp = rightElement.timestamp;
			if (rightElement.mark == PLUS_MARK) {
				processRightPlus(rightElement);
			} else if (rightElement.mark == MINUS_MARK) {
				processRightMinus(rightElement);
			} else if (rightElement.mark == '0') {
				processReset();
			}
			TimeCounterUtility::getInstance()->pause();
		} else if (!leftInputQueue->isEmpty()) {
			TimeCounterUtility::getInstance()->start();
			leftInputQueue->dequeue();
			//lastLeftTimestamp = leftElement.timestamp;
			if (leftElement.mark == PLUS_MARK) {
				processLeftPlus(leftElement);
			}
			if (leftElement.mark == MINUS_MARK) {
				processLeftMinus(leftElement);
			}
			TimeCounterUtility::getInstance()->pause();
		} else if (!rightInputQueue->isEmpty()) {
			TimeCounterUtility::getInstance()->start();
			rightInputQueue->dequeue();
			//lastRightTimestamp = rightElement.timestamp;
			if (rightElement.mark == PLUS_MARK) {
				processRightPlus(rightElement);
			} else if (rightElement.mark == MINUS_MARK) {
				processRightMinus(rightElement);
			} else if (rightElement.mark == '0') {
				processReset();
			}
			TimeCounterUtility::getInstance()->pause();
		}
	}
#ifdef DEBUG
	std::cout<<"===================operator over================="<<std::endl;
#endif
}
void JoinOperator::processLeftPlus(Element& leftElement) {

	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();
	Document& leftDocument = leftElement.document;
	if (this->leftRelationSynopsis->isFull()) {
		throw runtime_error("left synopsis is full");
	}
	this->leftRelationSynopsis->insertElement(leftElement);
	//cout << "leftPlusDocument: " << leftDocument << endl;
	this->rightElementIterator->initial();
	Element rightElement;

	//todo for test...

	double leftValue = boost::any_cast<int>(
			this->leftJoinAttribute->getValue(leftDocument));
	string sqlStmt = "select a, b from test where a = "
			+ boost::lexical_cast<string>(leftValue);
	Statement *stmt =
			DBManager::getInstance()->getDBConnection()->createStatement(
					sqlStmt);
	ResultSet *rset = stmt->executeQuery();
	while (rset->next()) {

		//right document
		DocumentBuilder documentBuilder;
		documentBuilder.append("A", rset->getInt(1));
		documentBuilder.append("B", rset->getInt(2));

		Document tmpTightDocument = documentBuilder.obj();
		Document& rightDocument = tmpTightDocument;

		boost::any retDocument = this->resultQueryProjection->performProjection(
				leftDocument, rightDocument);
		assert(retDocument.type() == typeid(Document));
		Document newDocument = boost::any_cast<Document>(retDocument);
		Element newElement;
		newElement.id =
				DocumentIdentifierGenerator::generateNewDocumentIdentifier();
		newElement.document = newDocument;
		newElement.mark = PLUS_MARK;
		newElement.timestamp = leftElement.timestamp;
		newElement.masterTag = leftElement.masterTag;
		//insert into lineage synopsis
		if (this->lineageSynopsis->isFull()) {
			throw runtime_error("lineage synopsis is full");
		}
		Lineage lineage;
		lineage.lineageNumber = 2;
		lineage.lineageDocumentId[0] = leftElement.id;
		lineage.lineageDocumentId[1] = rightElement.id;
		this->lineageSynopsis->insertLineage(lineage, newElement);

		//generate new output to the output queue
		if (outputQueue->isFull()) {
			throw std::runtime_error("output queue is full");
		}
		outputQueue->enqueue(newElement);

	}
	DBManager::getInstance()->getDBConnection()->terminateStatement(stmt);
	assert(!outputQueue->isFull());

}
void JoinOperator::processLeftMinus(Element& leftElement) {
	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();
	Document& leftDocument = leftElement.document;
	this->leftRelationSynopsis->deleteElement(leftElement);
	this->rightElementIterator->initial();
	//cout << "leftMinusDocument: " << leftDocument << endl;

	Element rightElement;
	//test

	//todo for test...
	double leftValue = boost::any_cast<int>(
			this->leftJoinAttribute->getValue(leftDocument));
	string sqlStmt = "select a, b from test where a = "
			+ boost::lexical_cast<string>(leftValue);
	Statement *stmt =
			DBManager::getInstance()->getDBConnection()->createStatement(
					sqlStmt);
	ResultSet *rset = stmt->executeQuery();
	while (rset->next()) {

		//right document
		DocumentBuilder documentBuilder;
		documentBuilder.append("A", rset->getInt(1));
		documentBuilder.append("B", rset->getInt(2));

		Document tmpRightDocument = documentBuilder.obj();
		Document& rightDocument = tmpRightDocument;

		//cout << "left-rightDocument: " << rightDocument << endl;

		Lineage lineage;
		lineage.lineageNumber = 2;
		lineage.lineageDocumentId[0] = leftElement.id;
		lineage.lineageDocumentId[1] = rightElement.id;
		Element elementInSynopsis;
		this->lineageSynopsis->getAndDeleteElement(lineage, elementInSynopsis);

		//generate output element
		Element outputElement;
		outputElement.mark = MINUS_MARK;
		outputElement.id = elementInSynopsis.id;
		outputElement.timestamp = leftElement.timestamp;
		outputElement.document = elementInSynopsis.document;
		outputElement.masterTag = leftElement.masterTag;
		//output element
		if (outputQueue->isFull()) {
			throw std::runtime_error("output queue is full");
		}
		outputQueue->enqueue(outputElement);

	}
	DBManager::getInstance()->getDBConnection()->terminateStatement(stmt);

	assert(!outputQueue->isFull());
}
void JoinOperator::processRightPlus(Element& rightElement) {
	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();
	Document rightDocument = rightElement.document;
	if (this->rightRelationSynopsis->isFull()) {
		throw runtime_error("right synopsis is full");
	}
	//this->rightRelationSynopsis->insertElement(rightElement);

	//cout << "rightPlusDocument: " << rightDocument << endl;

	this->leftElementIterator->initial();
	Element leftElement;
	while ((!outputQueue->isFull())
			&& this->leftElementIterator->getNext(leftElement)) {

		Document leftDocument = leftElement.document;

		boost::any leftValue = this->leftJoinAttribute->getValue(leftDocument);
		boost::any rightValue = this->rightJoinAttribute->getValue(
				rightDocument);
		//wangyan
		if (!QueryUtility::compareEqual(leftValue, rightValue)) {
			//continue;
		}
		boost::any retDocument = this->resultQueryProjection->performProjection(
				leftDocument, rightDocument);
		assert(retDocument.type() == typeid(Document));
		Document newDocument = boost::any_cast<Document>(retDocument);
		Element newElement;
		newElement.id =
				DocumentIdentifierGenerator::generateNewDocumentIdentifier();
		newElement.document = newDocument;
		newElement.mark = PLUS_MARK;
		newElement.timestamp = rightElement.timestamp;
		newElement.masterTag = rightElement.masterTag;
		//insert into lineage synopsis
		if (this->lineageSynopsis->isFull()) {
			throw runtime_error("lineage synopsis is full");
		}
		Lineage lineage;
		lineage.lineageNumber = 2;
		lineage.lineageDocumentId[0] = leftElement.id;
		lineage.lineageDocumentId[1] = rightElement.id;
		this->lineageSynopsis->insertLineage(lineage, newElement);

		//generate new output to the output queue
		if (outputQueue->isFull()) {
			throw std::runtime_error("output queue is full");
		}
		outputQueue->enqueue(newElement);
	}

	assert(!outputQueue->isFull());
}

void JoinOperator::processRightMinus(Element& rightElement) {
	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();
	Document& rightDocument = rightElement.document;
	//this->rightRelationSynopsis->deleteElement(rightElement);
	this->leftElementIterator->initial();
	//cout << "rightMinusDocument: " << rightDocument << endl;
	Element leftElement;
	while ((!outputQueue->isFull())
			&& this->leftElementIterator->getNext(leftElement)) {

		Document& leftDocument = leftElement.document;
		//if satisfied join condition
		//std::cout<<"left document : "<<leftDocument<<std::endl;
		//std::cout<<"right document : "<<rightDocument<<std::endl;
		boost::any leftValue = this->leftJoinAttribute->getValue(leftDocument);
		boost::any rightValue = this->rightJoinAttribute->getValue(
				rightDocument);
		//wangyan
		if (!QueryUtility::compareEqual(leftValue, rightValue)) {
			continue;
		}
		Lineage lineage;
		lineage.lineageNumber = 2;
		lineage.lineageDocumentId[0] = leftElement.id;
		lineage.lineageDocumentId[1] = rightElement.id;
		Element elementInSynopsis;
		this->lineageSynopsis->getAndDeleteElement(lineage, elementInSynopsis);
		//generate output element
		Element outputElement;
		outputElement.mark = MINUS_MARK;
		outputElement.id = elementInSynopsis.id;
		outputElement.timestamp = rightElement.timestamp;
		outputElement.document = elementInSynopsis.document;
		outputElement.masterTag = rightElement.masterTag;
		//output element
		if (outputQueue->isFull()) {
			throw std::runtime_error("output queue is full");
		}
		outputQueue->enqueue(outputElement);
	}
	assert(!outputQueue->isFull());
}

void JoinOperator::processReset() {

	long long thisTimestamp = TimestampGenerator::getCurrentTime();

	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();
	//output a reset element
	Element outputElement;
	outputElement.mark = '0';
	outputElement.id =
			DocumentIdentifierGenerator::generateNewDocumentIdentifier();
	outputElement.timestamp = thisTimestamp;

	string data = "{\"op\":\"reset\"}";
	//cout << "document: " << data << endl;
	//then process
	Document dbdataDoc = fromjson(data);

	outputElement.document = dbdataDoc;
	outputElement.masterTag = false;
	//output element
	if (outputQueue->isFull()) {
		throw std::runtime_error("output queue is full");
	}
	outputQueue->enqueue(outputElement);

	Element leftElement;
	assert(!outputQueue->isFull());

	int arraySize = 100;
	int aBuf[arraySize];
	int bBuf[arraySize];
	ub2 aLen[arraySize];
	ub2 bLen[arraySize];

	Statement *stmtInsert = DBManager::getInstance()->getStmtInsert();
	stmtInsert->setDataBuffer(1, aBuf, OCCIINT, sizeof(aBuf[0]), aLen);
	stmtInsert->setDataBuffer(2, bBuf, OCCIINT, sizeof(bBuf[0]), bLen);

	int count = 0;
	this->leftElementIterator->initial();
	while ((!outputQueue->isFull())
			&& this->leftElementIterator->getNext(leftElement)) {

		Document& leftDocument = leftElement.document;
		aBuf[count] = leftDocument.getField("A").Int();
		bBuf[count] = leftDocument.getField("B").Int();
		aLen[count] = sizeof(aBuf[count]);
		bLen[count] = sizeof(bBuf[count]);

		if (count == arraySize) {
			stmtInsert->executeArrayUpdate(count);
			count = 0;
		}

		count++;
	}
	//deal with the left
	if (count != 0) {
		stmtInsert->executeArrayUpdate(count);
	}
	DBManager::getInstance()->getDBConnection()->commit();
	//join table with table

	ResultSet *rset = DBManager::getInstance()->getStmtSelect()->executeQuery();
	int rsetsize = 0;
	while (rset->next()) {
		rsetsize++;
		//right document
		DocumentBuilder documentBuilder;
		documentBuilder.append("A", rset->getInt(1));
		documentBuilder.append("B", rset->getInt(2));
		documentBuilder.append("A2", rset->getInt(3));

		//generate output element
		Element outputElement;
		outputElement.mark = MINUS_MARK;
		outputElement.id =
				DocumentIdentifierGenerator::generateNewDocumentIdentifier();
		;
		outputElement.timestamp = thisTimestamp;
		outputElement.document = documentBuilder.obj();
		outputElement.masterTag = false;
		//output element
		if (outputQueue->isFull()) {
			throw std::runtime_error("output queue is full");
		}
		outputQueue->enqueue(outputElement);

	}
	cout << "rsetsize: " << rsetsize << endl;
	DBManager::getInstance()->getStmtDelete()->executeUpdate();

	DBManager::getInstance()->getDBConnection()->commit();

}

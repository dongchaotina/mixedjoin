#pragma once
#include "../Common/stdafx.h"
#include "../Operator/Operator.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../IO/IStreamOutput.h"
#include "../Query/QueryEntity.h"
#include <boost/shared_ptr.hpp>
class RootOperator:public Operator
{
private:
	boost::shared_ptr<IStreamOutput>streamOutput;
	QueryEntity* queryEntity;
public:
	RootOperator(void);
	~RootOperator(void);
	void execution();
	
	void setStreamOutput(boost::shared_ptr<IStreamOutput> streamOutput);
	void setQueryEntity(QueryEntity* queryEntity);
	QueryEntity* getQueryEntity();
};


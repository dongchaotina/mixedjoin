/*
 * RelationOperator.cpp
 *
 *  Created on: May 21, 2015
 *      Author: root
 */

#include "RelationLeafOperator.h"

#include <iostream>
#include <sstream>
#include <memory>
#include <string>
#include <stdexcept>
#include <string>

#include <netinet/in.h>
#include <sys/socket.h>
#include <fcntl.h>

#include <event2/event.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/thread.h>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/any.hpp>
#include "../BinaryJson/json.h"

#include "../Utility/DocumentIdentifierGenerator.h"
#include "../Utility/TimestampGenerator.h"
#include "../Utility/TimeCounterUtility.h"
#include "../Query/QueryUtility.h"

using namespace std;
using namespace oracle::occi;
using namespace ttree;

#include <occi.h>

void run(RelationLeafOperator * relationLeafOperator);
void read_callback_update(struct bufferevent *bev, void *ctx);
void errorcb(struct bufferevent *bev, short error, void *ctx);
void do_accept(evutil_socket_t listener, short event, void *arg);

struct event_base *base;
Environment *env;
Connection *conn;
Statement *stmt;
boost::shared_ptr<QueueEntity> outputQueue;
long long lastTimestamp = TimestampGenerator::getCurrentTime();
std::string lastSQLHash = "";

int tableSize = 500000;

RelationLeafOperator::RelationLeafOperator() {

}

RelationLeafOperator::~RelationLeafOperator() {
	event_base_loopexit(base, NULL);
}

std::string getSqlFromSchema(boost::shared_ptr<JsonSchema> schema,
		std::string tableName);
Document generateDocumentFromResult(Document propertiesDocument,
		ResultSet * res);

boost::shared_ptr<TTreeSynopsis> RelationLeafOperator::getJoinSynopsis() {
	//get join operator
	boost::shared_ptr<Operator> op =
			this->getOutputQueueList().back()->getOutputOperator();
	boost::shared_ptr<TTreeJoinOperator> tOP = boost::static_pointer_cast<
			TTreeJoinOperator>(op);
	boost::shared_ptr<TTreeSynopsis> rightTTreeSynopsis =
			tOP->getRightTTreeSynopsis();
	return rightTTreeSynopsis;
}

void RelationLeafOperator::execution() {

	//output all elements once
	/*env = Environment::createEnvironment(Environment::DEFAULT);
	 conn = env->createConnection(relationInput->getUserName(),
	 relationInput->getUserPassword(), relationInput->getDatabaseName());
	 outputQueue = this->getOutputQueueList().front();

	 Document schemaDocument = relationInput->getSchema()->getSchemaDocument();
	 Document propertiesDocument =
	 schemaDocument.getField(SCHEMA_PROPERTIES).embeddedObject();

	 string sqlStmt = getSqlFromSchema(relationInput->getSchema(),
	 relationInput->getTableName());

	 stmt = conn->createStatement(sqlStmt);
	 cout << "output table start: " << endl;
	 ResultSet *rset = stmt->executeQuery();

	 long long timestamp = TimestampGenerator::getCurrentTime();

	 boost::shared_ptr<TTreeSynopsis> rightTTreeSynopsis = getJoinSynopsis();

	 int i = 0;

	 while (rset->next()) {

	 i++;
	 if (i % 10000 == 0) {
	 cout << i << endl;
	 }

	 Document document =
	 generateDocumentFromResult(propertiesDocument, rset);
	 Element element;
	 DocumentId thisElementId =
	 DocumentIdentifierGenerator::generateNewDocumentIdentifier();

	 element.id = thisElementId;
	 element.mark = PLUS_MARK;
	 element.timestamp = timestamp;
	 element.document = document;
	 element.document.getOwned();

	 //outputQueue->enqueue(element);
	 rightTTreeSynopsis->insertElement(element);

	 }

	 //then listening to the changes in the rdbms
	 setvbuf(stdout, NULL, _IONBF, 0);
	 boost::thread listenThread(run, this);*/

	//this->byGenerate();

	cout << "output table end " << endl;

}

void generateIncrement(RelationLeafOperator * relationLeafOperator) {

	long long timestamp = TimestampGenerator::getCurrentTime();
	boost::shared_ptr<TTreeSynopsis> rightTTreeSynopsis =
			relationLeafOperator->getJoinSynopsis();
	outputQueue = relationLeafOperator->getOutputQueueList().front();
	while (true) {

		DocumentBuilder documentBuilder;
		documentBuilder.append("A", rand() % 1000);
		documentBuilder.append("B", rand() % 1000);
		documentBuilder.append("PK", tableSize + 1);
		Document document = documentBuilder.obj();

		Element element;
		DocumentId thisElementId =
				DocumentIdentifierGenerator::generateNewDocumentIdentifier();

		element.id = thisElementId;
		element.mark = PLUS_MARK;
		element.timestamp = timestamp;
		element.document = document;
		element.document.getOwned();

		outputQueue->enqueue(element);

		element.mark = MINUS_MARK;
		outputQueue->enqueue(element);

		SleepUtility::sleepMicroSecond(100 * 10000);

	}

}

void RelationLeafOperator::byGenerate() {

	long long timestamp = TimestampGenerator::getCurrentTime();

	boost::shared_ptr<TTreeSynopsis> rightTTreeSynopsis = getJoinSynopsis();

	for (int i = 0; i < tableSize; i++) {

		if (i % 10000 == 0) {
			cout << i << endl;
		}

		DocumentBuilder documentBuilder;
		documentBuilder.append("A", rand() % 1000);
		documentBuilder.append("B", rand() % 1000);
		documentBuilder.append("PK", i);
		Document document = documentBuilder.obj();

		Element element;
		DocumentId thisElementId =
				DocumentIdentifierGenerator::generateNewDocumentIdentifier();

		element.id = thisElementId;
		element.mark = PLUS_MARK;
		element.timestamp = timestamp;
		element.document = document;
		element.document.getOwned();

		rightTTreeSynopsis->insertElement(element);

	}

	//then listening to the changes in the rdbms
	setvbuf(stdout, NULL, _IONBF, 0);
	boost::thread listenThread(generateIncrement, this);

}

std::string getSqlFromSchema(boost::shared_ptr<JsonSchema> schema,
		std::string tableName) {
	Document schemaDocument = schema->getSchemaDocument();
	Document propertiesDocument =
			schemaDocument.getField(SCHEMA_PROPERTIES).embeddedObject();

	DocumentIterator it(propertiesDocument);

	std::string sql = "select ";

	while (it.more()) {
		DocumentElement documentElement = it.next();
		std::string fieldName = documentElement.fieldName();
		sql += fieldName + ", ";
	}
	sql.erase(sql.find_last_of(","));
	sql += " from " + tableName;

	return sql;

}

Document generateDocumentFromResult(Document propertiesDocument,
		ResultSet * res) {

	DocumentBuilder documentBuilder;
	DocumentIterator it(propertiesDocument);
	int i = 1;
	while (it.more()) {
		DocumentElement documentElement = it.next();
		std::string fieldName = documentElement.fieldName();
		Document fieldDocument = documentElement.embeddedObject();
		std::string fieldType = std::string(
				fieldDocument.getField(SCHEMA_TYPE).valuestr());
		JSONTYPE jsonType = generateJsonType(fieldType);

		if (jsonType == JSON_TRUE) {
			documentBuilder.appendBool(fieldName, true);
		} else if (jsonType == JSON_FALSE) {
			documentBuilder.appendBool(fieldName, false);
		} else if (jsonType == JSON_ARRAY) {
			Document nestedDocument;
			Document nestedItemsDocument =
					fieldDocument.getField(SCHEMA_ITEMS).embeddedObject();
			documentBuilder.append(fieldName, nestedDocument);
		} else if (jsonType == JSON_NULL) {
			documentBuilder.appendBool(fieldName, NULL);
		} else if (jsonType == JSON_NUMBER) {
			int value = res->getInt(i++);
			documentBuilder.append(fieldName, value);
		} else if (jsonType == JSON_OBJECT) {
			Document nestedDocument;
			Document nestedPropertiesDocument = fieldDocument.getField(
					SCHEMA_PROPERTIES).embeddedObject();
			documentBuilder.append(fieldName, nestedDocument);
		} else if (jsonType == JSON_STRING) {
			string str = res->getString(i++);
			documentBuilder.append(fieldName, str);
		}

	}
	return documentBuilder.obj();
}

void read_callback_update(struct bufferevent *bev, void *ctx) {
	RelationLeafOperator * thisOp = (RelationLeafOperator *) ctx;

	char tmp[256];
	//read +/- data
	struct evbuffer *input;
	input = bufferevent_get_input(bev);
	size_t buffer_len = evbuffer_get_length(input);

	if (buffer_len <= 0) {
		bufferevent_free(bev);
		return;
	}
	evbuffer_remove(input, tmp, buffer_len);
	string data(&tmp[0], &tmp[buffer_len]);
	bufferevent_free(bev);
	//then process
	Document dbdataDoc = fromjson(data);

	cout << "dbdataDoc: " << dbdataDoc << endl;

	std::string thisSQLHash = dbdataDoc.getField("hash").valuestr();
	std::string mark = dbdataDoc.getField("mark").valuestr();

	if (thisSQLHash != lastSQLHash) {
		lastSQLHash = thisSQLHash;
		lastTimestamp = TimestampGenerator::getCurrentTime();
	}

	if (dbdataDoc.hasField("minus")) {

		/*boost::recursive_mutex::scoped_lock lock(thisOp->m_oMutex);
		 thisOp->relationInput->relationTuplesNumber++;
		 lock.unlock();*/

		Document minusDoc = dbdataDoc.getField("minus").embeddedObject();
		int pk = minusDoc.getField("PK").Int();

		boost::shared_ptr<TTreeSynopsis> rightTTreeSynopsis =
				thisOp->getJoinSynopsis();
		Element element;
		rightTTreeSynopsis->buffer->findElementByKey(pk, NULL, element);

		element.mark = MINUS_MARK;
		element.timestamp = lastTimestamp;
		//element.document = minusDoc;
		//element.document.getOwned();

		outputQueue->enqueue(element);

	}

	if (dbdataDoc.hasField("plus")) {

		/*boost::recursive_mutex::scoped_lock lock(thisOp->m_oMutex);
		 thisOp->relationInput->relationTuplesNumber++;
		 lock.unlock();*/

		Document plusDoc = dbdataDoc.getField("plus").embeddedObject();
		DocumentId thisElementId =
				DocumentIdentifierGenerator::generateNewDocumentIdentifier();

		Element element;
		element.id = thisElementId;
		element.mark = PLUS_MARK;
		element.timestamp = lastTimestamp;
		element.document = plusDoc;
		element.document.getOwned();

		outputQueue->enqueue(element);

		/*memset(tmp, '\0', sizeof(tmp));
		 char tmp2[10];
		 string strElementId = boost::lexical_cast<string>(thisElementId);
		 strcpy(tmp2, strElementId.c_str());
		 evbuffer_add(bufferevent_get_output(bev), "0", strlen("0"));*/

	}

}

void write_callback_update(struct bufferevent *bev, void *ctx) {

}

void errorcb(struct bufferevent *bev, short error, void *ctx) {
	if (error & BEV_EVENT_EOF) {
		/* connection has been closed, do any clean up here */
		bufferevent_free(bev);
	} else if (error & BEV_EVENT_ERROR) {
		/* check errno to see what error occurred */
		printf("some other error\n");
		bufferevent_free(bev);
		//event_base_loopexit(base, NULL);
	} else if (error & BEV_EVENT_TIMEOUT) {
		/* must be a timeout event handle, handle it */
		printf("Timed out\n");
		bufferevent_free(bev);
		//event_base_loopexit(base, NULL);
	}

}

void do_accept(evutil_socket_t listener, short event, void *arg) {
	RelationLeafOperator * thisOp = (RelationLeafOperator *) arg;
	struct sockaddr_storage ss;
	socklen_t slen = sizeof(ss);

	int fd = accept(listener, (struct sockaddr*) &ss, &slen);
	if (fd < 0) {
		perror("accept");
	} else if (fd > FD_SETSIZE) {
		close(fd);
	} else {
		struct bufferevent *bev;

		bev = bufferevent_socket_new(base, fd, BEV_OPT_CLOSE_ON_FREE);

		bufferevent_setcb(bev, read_callback_update, NULL, errorcb, thisOp);
		bufferevent_enable(bev, EV_READ | EV_WRITE);
	}
}

void run(RelationLeafOperator * relationLeafOperator) {

	cout << "listening starts" << endl;

	evutil_socket_t listener;
	struct sockaddr_in sin;

	struct event *listener_event;
	//evthread_use_pthreads();
	base = event_base_new();
	if (!base)
		return; /*XXXerr*/

	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = inet_addr("192.168.2.234");
	sin.sin_port = htons(23456);

	listener = socket(AF_INET, SOCK_STREAM, 0);
	evutil_make_socket_nonblocking(listener);

	if (::bind(listener, (struct sockaddr*) &sin, sizeof(sin)) < 0) {
		perror("bind");
		return;
	}
	if (listen(listener, 16) < 0) {
		perror("listen");
		return;
	}

	listener_event = event_new(base, listener, EV_WRITE | EV_READ | EV_PERSIST,
			do_accept, (void*) relationLeafOperator);
	/*XXX check it */
	event_add(listener_event, NULL);

	event_base_dispatch(base);
}

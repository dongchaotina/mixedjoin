#pragma once
#include "../Common/stdafx.h"
#include "../Operator/Operator.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Internal/Synopsis/LineageSynopsis.h"
#include "../Operator/Operator.h"
#include "../Query/QueryProjectionAbstract.h"
#include <boost/shared_ptr.hpp>
class ProjectionOperator:public Operator
{
private:
	boost::shared_ptr<QueryProjectionAbstract> queryProjection;
//	std::set<std::string> projectionAttributeSet;
	boost::shared_ptr<LineageSynopsis>lineageSynopsis;
public:
	
	ProjectionOperator(boost::shared_ptr<QueryProjectionAbstract> queryProjection);
	~ProjectionOperator(void);
	
	void execution();
	 
};


#pragma once
#include "../Common/stdafx.h"
#include "../Operator/Operator.h"
#include "../Internal/Synopsis/WindowSynopsis.h"
#include "../Internal/Queue/QueueEntity.h"
#include <boost/shared_ptr.hpp>
class RowWindowOperator:public Operator
{
private:
	const int rowWindowSize;
	//int rowWindowSize;
	int currentRowNumberInSynopsis;
	boost::shared_ptr<WindowSynopsis> windowSynopsis;
	Timestamp lastInputTimestamp;
	Element blockedElement;

public:
	RowWindowOperator(int rowWindowSize);
	~RowWindowOperator(void);

    void execution();
	//void dealWithBlock(void);
	int getWindowSize();
};


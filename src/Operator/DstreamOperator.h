#pragma once
#include "../Common/stdafx.h"
#include "../Operator/Operator.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Internal/Synopsis/LineageSynopsis.h"
#include "../Internal/Synopsis/RelationSynopsis.h"
#include "../Operator/Operator.h"
#include "../Internal/Element/ElementIterator.h"
#include <boost/shared_ptr.hpp>
class DstreamOperator:public Operator 
{
private:
	
	boost::shared_ptr<RelationSynopsis>relationSynopsis;
	boost::shared_ptr<ElementIterator> elementIterator;
	Timestamp lastInputTimestamp;
	Element blockElement;
	void produceOutput();
	
public:
	DstreamOperator(void);
	~DstreamOperator(void);
	void execution();
	
};


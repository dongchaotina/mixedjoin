#include "../Common/stdafx.h"
#include "../Operator/RootOperator.h"
#include "../Utility/TimeCounterUtility.h"
#include "../Scheduler/Scheduler.h"
RootOperator::RootOperator(void)
{
}


RootOperator::~RootOperator(void)
{
}
void RootOperator::execution()
{
#ifdef DEBUG
	std::cout<<"===================operator begin================="<<std::endl;
	std::cout<<"operatorid : "<<this->getId()<<std::endl;
#endif
	assert(this->getInputQueueList().size()==1);
	boost::shared_ptr<QueueEntity>inputQueue = this->getInputQueueList().front();
	while(1)
	{
		if(inputQueue->isEmpty())
		{
			break;
		}
		if(this->streamOutput->isFull())
		{
			break;
		}
		TimeCounterUtility::getInstance()->start();
		Element inputElement;
		inputQueue->dequeue(inputElement);
		if(inputElement.masterTag==true)
		{
			int test;
			test = 3;
		}
		this->streamOutput->pushNext(inputElement);

		TimeCounterUtility::getInstance()->pause();
	}
#ifdef DEBUG
	std::cout<<"===================operator over================="<<std::endl;
#endif
}

void RootOperator::setStreamOutput(boost::shared_ptr<IStreamOutput> streamOutput)
{
	this->streamOutput = streamOutput;
	this->streamOutput->initial();
}
void RootOperator::setQueryEntity( QueryEntity* queryEntity)
{
	this->queryEntity = queryEntity;
}
QueryEntity* RootOperator::getQueryEntity()
{
	return this->queryEntity;
}

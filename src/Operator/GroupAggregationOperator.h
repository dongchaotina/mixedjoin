#pragma once
#include "../Common/stdafx.h"
#include "../Operator/Operator.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Internal/Synopsis/LineageSynopsis.h"
#include "../Internal/Synopsis/RelationSynopsis.h"
#include "../Operator/Operator.h"
#include "../Internal/Element/ElementIterator.h"
#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>

#include "../Query/QueryProjectionAbstract.h"
#include <boost/any.hpp>

class GroupAggregationOperator: public Operator {
private:
	boost::shared_ptr<RelationSynopsis> relationSynopsis;
	boost::shared_ptr<LineageSynopsis> lineageSynopsis;
	std::map<int, std::set<DocumentId> > groupMap;
	std::string groupByAttributeName;
	boost::shared_ptr<QueryProjectionAbstract> resultQueryProjection;

public:
	GroupAggregationOperator(void);
	~GroupAggregationOperator(void);
	void execution();
	void setGroupByAttributeName(std::string groupByAttributeName);
	void setAggregationAttributeName(std::string aggregationAttributeName);

	void setResultQueryProjection(
			boost::shared_ptr<QueryProjectionAbstract> resultQueryProjection);

	std::multimap<std::string, AggregateOperation> getAggregattionMap();

};


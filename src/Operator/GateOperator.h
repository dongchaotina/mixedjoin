#include "../Common/stdafx.h"
#include "../Operator/Operator.h"
#include "../Query/QueryEntity.h"
#include "../Internal/Synopsis/RelationSynopsis.h"
#include <boost/shared_ptr.hpp>
#include <list>
class GateOperator: public Operator
{
private:
	static int gateOperatorNumber;
	std::list<QueryEntity* > queryEntityList;
	boost::shared_ptr<RelationSynopsis> relationSynopsis;
public:
	static int deletedNumber;
	static int totalPlusNumber;
	GateOperator();
	~GateOperator();
	void execution();
	void addQueryEntity(QueryEntity* queryEntity);
		

};


/*
 * GroupbyOperator.cpp
 *
 *  Created on: Nov 24, 2015
 *      Author: root
 */

#include "GroupbyOperator.h"
#include "../Common/stdafx.h"
#include "../Internal/Synopsis/LineageSynopsis.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Utility/DocumentIdentifierGenerator.h"
#include "../Internal/Synopsis/RelationSynopsis.h"
#include "../Internal/Element/ElementIterator.h"
#include "../Utility/TimeCounterUtility.h"
#include "../Scheduler/Scheduler.h"

namespace ttree {

GroupbyOperator::GroupbyOperator() {
	// TODO Auto-generated constructor stub
	this->relationSynopsis.reset(new RelationSynopsis());
	this->groupByAttributeName = "";
}

GroupbyOperator::~GroupbyOperator() {
	// TODO Auto-generated destructor stub
}

std::multimap<std::string, AggregateOperation> GroupAggregationOperator::getAggregattionMap() {

	//for aggregationAttributeName and operation
	std::multimap<std::string, AggregateOperation> aggregattionMap;

	boost::shared_ptr<ObjectQueryProjection> objectQueryProjection =
			boost::static_pointer_cast<ObjectQueryProjection>(
					this->resultQueryProjection);

	std::vector<boost::shared_ptr<QueryProjectionAbstract> > fields =
			objectQueryProjection->getProjectionFields();

	vector<boost::shared_ptr<QueryProjectionAbstract> >::iterator iter =
			fields.begin();
	while (iter != fields.end()) {

		boost::shared_ptr<QueryProjectionAbstract> queryProjectionAbstract =
				*iter;
		boost::shared_ptr<DirectQueryProjection> directQueryProjection =
				boost::static_pointer_cast<DirectQueryProjection>(
						queryProjectionAbstract);
		boost::shared_ptr<QueryExpressionAbstract> expression =
				directQueryProjection->getExpression();
		boost::shared_ptr<AggregationQueryExpression> aggregationQueryExpression =
				boost::static_pointer_cast<AggregationQueryExpression>(
						expression);

		AttrSource attrSource =
				aggregationQueryExpression->getAttributeSource();

		switch (attrSource) {

		case _GROUP_KEY_VAR: {
			//no need
			//this->groupByAttributeName = aggregationAttributeName;
			break;
		}
		case _GROUP_ARRAY: {

			std::string aggregationAttributeName =
					aggregationQueryExpression->getAggregationAttributeName();
			AggregateOperation aggregateOperation =
					aggregationQueryExpression->getOperation();
			aggregattionMap.insert(
					map<std::string, AggregateOperation>::value_type(
							aggregationAttributeName, aggregateOperation));

			break;
		}

		}

		iter++;
	}

	return aggregattionMap;
}

void GroupbyOperator::execution() {

	assert(this->getInputQueueList().size() == 1);
	assert(this->getOutputQueueList().size() == 1);

	boost::shared_ptr<QueueEntity> inputQueue =
			this->getInputQueueList().front();
	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();

	aggregattionMap = this->getAggregattionMap();

	while (1) {

		Element inputElement;
		if (outputQueue->isFull()) {
			break;
		}
		if (inputQueue->isEmpty()) {
			break;
		}
		inputQueue->dequeue(inputElement);

		if (inputElement.mark == PLUS_MARK) {
			processPlus(inputElement);
		} else {
			processMinus(inputElement);
		}

	}
}

int GroupbyOperator::getGBAttributeValue(Document& inputDocument) {
	if (this->groupByAttributeName != "") {
		return inputDocument.getField(this->groupByAttributeName).Int();
	} else {
		return 99999;
	}
}

void GroupbyOperator::processPlus(Element& inputElement) {

	if (this->relationSynopsis->isFull()) {
		throw runtime_error("left synopsis is full");
	}
	//insert into the relation synopsis
	this->relationSynopsis->insertElement(inputElement);

	Document& inputDocument = inputElement.document;
	int groupByAttributeValue = getGBAttributeValue(inputDocument);

	std::map<int, std::map<AggregateOperation, double> >::iterator it =
			groupMap.find(groupByAttributeValue);

	if (it != this->groupMap.end()) {
		// group already exist
		std::map<AggregateOperation, double> valueMap = it->second;
		std::multimap<std::string, AggregateOperation>::iterator iter =
				aggregattionMap.begin();
		while (iter != aggregattionMap.end()) {

			std::string aggregationAttributeName = iter->first;
			AggregateOperation aggregateOperation = iter->second;




		}

	} else {
		// group not exist
		std::set<DocumentId> groupSet;
		groupSet.insert(inputElement.id);
		this->groupMap.insert(make_pair(groupByAttributeValue, groupSet));
	}

}
void GroupbyOperator::processMinus(Element& element) {

}

} /* namespace ttree */

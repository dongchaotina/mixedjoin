#include "../Common/stdafx.h"
#include "../Operator/GroupAggregationOperator.h"
#include "../Internal/Synopsis/LineageSynopsis.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Utility/DocumentIdentifierGenerator.h"
#include "../Internal/Synopsis/RelationSynopsis.h"
#include "../Internal/Element/ElementIterator.h"
#include "../Utility/TimeCounterUtility.h"
#include "../Scheduler/Scheduler.h"

GroupAggregationOperator::GroupAggregationOperator(void) {
	int lineageNumber = 1;
	this->lineageSynopsis.reset(new LineageSynopsis(lineageNumber));
	this->relationSynopsis.reset(new RelationSynopsis());
	this->groupByAttributeName = "";
}

GroupAggregationOperator::~GroupAggregationOperator(void) {
}

void GroupAggregationOperator::setResultQueryProjection(
		boost::shared_ptr<QueryProjectionAbstract> resultQueryProjection) {
	this->resultQueryProjection = resultQueryProjection;
}

void GroupAggregationOperator::setGroupByAttributeName(
		std::string groupByAttributeName) {
	this->groupByAttributeName = groupByAttributeName;
}

std::multimap<std::string, AggregateOperation> GroupAggregationOperator::getAggregattionMap() {

	//for aggregationAttributeName and operation
	std::multimap<std::string, AggregateOperation> aggregattionMap;

	boost::shared_ptr<ObjectQueryProjection> objectQueryProjection =
			boost::static_pointer_cast<ObjectQueryProjection>(
					this->resultQueryProjection);

	std::vector<boost::shared_ptr<QueryProjectionAbstract> > fields =
			objectQueryProjection->getProjectionFields();

	vector<boost::shared_ptr<QueryProjectionAbstract> >::iterator iter =
			fields.begin();
	while (iter != fields.end()) {

		boost::shared_ptr<QueryProjectionAbstract> queryProjectionAbstract =
				*iter;
		boost::shared_ptr<DirectQueryProjection> directQueryProjection =
				boost::static_pointer_cast<DirectQueryProjection>(
						queryProjectionAbstract);
		boost::shared_ptr<QueryExpressionAbstract> expression =
				directQueryProjection->getExpression();
		boost::shared_ptr<AggregationQueryExpression> aggregationQueryExpression =
				boost::static_pointer_cast<AggregationQueryExpression>(
						expression);

		AttrSource attrSource =
				aggregationQueryExpression->getAttributeSource();

		switch (attrSource) {

		case _GROUP_KEY_VAR: {
			//no need
			//this->groupByAttributeName = aggregationAttributeName;
			break;
		}
		case _GROUP_ARRAY: {

			std::string aggregationAttributeName =
					aggregationQueryExpression->getAggregationAttributeName();
			AggregateOperation aggregateOperation =
					aggregationQueryExpression->getOperation();
			aggregattionMap.insert(
					map<std::string, AggregateOperation>::value_type(
							aggregationAttributeName, aggregateOperation));

			break;
		}

		}

		iter++;
	}

	return aggregattionMap;
}

void GroupAggregationOperator::execution() {

#ifdef DEBUG
	std::cout<<"===================operator begin================="<<std::endl;
	std::cout<<"operatorid : "<<this->getId()<<std::endl;
#endif
	assert(this->getInputQueueList().size() == 1);
	assert(this->getOutputQueueList().size() == 1);
	boost::shared_ptr<QueueEntity> inputQueue =
			this->getInputQueueList().front();
	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();

	std::multimap<std::string, AggregateOperation> aggregattionMap =
			this->getAggregattionMap();


	while (1) {

		// do aggregation 1st, then do projection, though the jaqlDocument...

		//1ST AGGREGATION
		Element inputElement;
		if (outputQueue->isFull()) {
			break;
		}
		if (inputQueue->isEmpty()) {
			break;
		}
		inputQueue->dequeue(inputElement);

		if (inputElement.mark == PLUS_MARK) {

			Document newDocument;
			DocumentBuilder newDocumentBuilder;

			Document& inputDocument = inputElement.document;
			if (this->relationSynopsis->isFull()) {
				throw runtime_error("left synopsis is full");
			}
			//insert into the relation synopsis
			this->relationSynopsis->insertElement(inputElement);

			//generate the result element
			//this should be able to handle with both string and number..and so on..
			int groupByAttributeValue;
			if (this->groupByAttributeName != "") {
				assert(
						inputDocument.hasField(
								this->groupByAttributeName.c_str()));
				groupByAttributeValue = inputDocument.getField(
						this->groupByAttributeName).Int();

				newDocumentBuilder.appendNumber(this->groupByAttributeName,
						groupByAttributeValue);
			} else {
				groupByAttributeValue = 99999;
			}

			//deal with the groupmap, if the group already exists, insert the element. 
			//if not exists, create the group and insert the element.

			std::set<DocumentId> currentGroupSet;
			std::map<int, std::set<DocumentId> >::iterator it = groupMap.find(
					groupByAttributeValue);
			if (it != this->groupMap.end()) {			// group already exist
				std::set<DocumentId>& groupSet = it->second;
				currentGroupSet = groupSet;
				groupSet.insert(inputElement.id);
			} else {			// group not exist
				std::set<DocumentId> groupSet;
				groupSet.insert(inputElement.id);
				this->groupMap.insert(
						make_pair(groupByAttributeValue, groupSet));
				currentGroupSet = groupSet;
			}

			//above just uses the groupByAttributeName.

			Element newElement;
			std::multimap<std::string, AggregateOperation>::iterator iter =
						aggregattionMap.begin();
			while (iter != aggregattionMap.end()) {
				std::string aggregationAttributeName = iter->first;
				AggregateOperation aggregateOperation = iter->second;

				//calculate the aggregation value
				boost::shared_ptr<SetElementIterator> elementIterator;
				elementIterator.reset(
						new SetElementIterator(this->relationSynopsis,
								currentGroupSet));
				elementIterator->initial();
				Element groupElement;
				double average = 0, sum = 0, count = 0;
				//how to get first element?
				elementIterator->getThis(groupElement);
				double max = groupElement.document.getField(
						aggregationAttributeName).Number();
				double min = max;
				while ((!outputQueue->isFull())
						&& elementIterator->getNext(groupElement)) {
					assert(
							groupElement.document.getField(
									aggregationAttributeName).isNumber());
					double dl = groupElement.document.getField(
							aggregationAttributeName).Number();

					if (max < dl) {
						max = dl;
					}
					if (min > dl) {
						min = dl;
					}

					sum += dl;
					count++;
				}
				average = sum / count;

				if (aggregateOperation == AVG) {
					newDocumentBuilder.appendNumber(
							"avg(" + aggregationAttributeName + ")", average);
				} else if (aggregateOperation == SUM) {
					newDocumentBuilder.appendNumber(
							"sum(" + aggregationAttributeName + ")", sum);
				} else if (aggregateOperation == COUNT) {
					newDocumentBuilder.appendNumber(
							"count(" + aggregationAttributeName + ")", count);
				} else if (aggregateOperation == MIN) {
					newDocumentBuilder.appendNumber(
							"min(" + aggregationAttributeName + ")", min);
				} else if (aggregateOperation == MAX) {
					newDocumentBuilder.appendNumber(
							"max(" + aggregationAttributeName + ")", max);
				}

				iter++;
			}

			newDocument = newDocumentBuilder.obj();

			//2ND DO PROJECTION
			boost::any retDocument =
					this->resultQueryProjection->performProjection(newDocument);
			newDocument = boost::any_cast<Document>(retDocument);

			newElement.id =
					DocumentIdentifierGenerator::generateNewDocumentIdentifier();
			newElement.document = newDocument;
			newElement.mark = PLUS_MARK;
			newElement.timestamp = inputElement.timestamp;
			newElement.masterTag = inputElement.masterTag;
			//insert into lineage synopsis
			if (this->lineageSynopsis->isFull()) {
				throw runtime_error("lineage synopsis is full");
			}
			Lineage lineage;
			lineage.lineageNumber = 1;
			lineage.lineageDocumentId[0] = inputElement.id;
			this->lineageSynopsis->insertLineage(lineage, newElement);

			//generate new output to the output queue
			if (outputQueue->isFull()) {
				throw std::runtime_error("output queue is full");
			}

			outputQueue->enqueue(newElement);

		} else if (inputElement.mark == MINUS_MARK) {
			//delete from relation synopsis
			relationSynopsis->deleteElement(inputElement);
			//delete from group map
			Document& inputDocument = inputElement.document;
			int groupByAttributeValue;
			if (this->groupByAttributeName != "") {
				assert(
						inputDocument.hasField(
								this->groupByAttributeName.c_str()));
				groupByAttributeValue = inputDocument.getField(
						this->groupByAttributeName).Int();
			} else {
				groupByAttributeValue = 99999;
			}
			std::map<int, std::set<DocumentId> >::iterator it =
					this->groupMap.find(groupByAttributeValue);
			assert(it != this->groupMap.end());
			std::set<DocumentId>& documentIdSet = it->second;
			documentIdSet.erase(inputElement.id);

			//delete from lineage synopsis
			Element elementInSynopsis;
			Lineage lineage;
			lineage.lineageNumber = 1;
			lineage.lineageDocumentId[0] = inputElement.id;
			int ret = lineageSynopsis->getAndDeleteElement(lineage,
					elementInSynopsis);
			if (ret == false) {
				//lineage not found, the minus tuple has already been generated
				//we needn't generate the minus tuple again
				continue;
				//throw std::runtime_error("lineage not exist");
			}
			//generate minus element
			Element outputElement;
			outputElement.mark = MINUS_MARK;
			outputElement.document = elementInSynopsis.document;
			outputElement.timestamp = inputElement.timestamp;
			outputElement.id = elementInSynopsis.id;
			outputElement.masterTag = inputElement.masterTag;
			outputQueue->enqueue(outputElement);
		}
	}
#ifdef DEBUG
	std::cout<<"===================operator over================="<<std::endl;
#endif
}

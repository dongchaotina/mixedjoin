#pragma once
#include "../Common/stdafx.h"
#include "../Operator/Operator.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Query/QueryConditionAbstract.h"
#include <boost/shared_ptr.hpp>
class SelectionOperator:public Operator
{
private:
	boost::shared_ptr<QueryConditionAbstract> condition;
	bool isSatisfySelectionCondition(Element& element);
public:
	SelectionOperator(void);
	~SelectionOperator(void);
	void execution();
	void setSelectionCondition(boost::shared_ptr<QueryConditionAbstract> condition);
};


/*
 * GroupbyOperator.h
 *
 *  Created on: Nov 24, 2015
 *      Author: root
 */

#ifndef SRC_OPERATOR_GROUPBYOPERATOR_H_
#define SRC_OPERATOR_GROUPBYOPERATOR_H_

#include "../Common/stdafx.h"
#include "../Operator/Operator.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Internal/Synopsis/LineageSynopsis.h"
#include "../Internal/Synopsis/RelationSynopsis.h"
#include "../Operator/Operator.h"
#include "../Internal/Element/ElementIterator.h"
#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>

#include "../Query/QueryProjectionAbstract.h"
#include <boost/any.hpp>

namespace ttree {

class GroupbyOperator: public Operator {

private:
	boost::shared_ptr<RelationSynopsis> relationSynopsis;
	std::map<int, std::map<AggregateOperation, double> > groupMap;
	std::multimap<std::string, AggregateOperation> aggregattionMap;
	std::string groupByAttributeName;
	boost::shared_ptr<QueryProjectionAbstract> resultQueryProjection;

	std::multimap<std::string, AggregateOperation> getAggregattionMap();

	void processPlus(Element& element);
	void processMinus(Element& element);
	int getGBAttributeValue(Document& inputDocument);
	std::set<DocumentId> getCurrentGroupSet(int groupByAttributeValue, Element& inputElement);

public:
	GroupbyOperator();
	virtual ~GroupbyOperator();
	void execution();

};

} /* namespace ttree */

#endif /* SRC_OPERATOR_GROUPBYOPERATOR_H_ */

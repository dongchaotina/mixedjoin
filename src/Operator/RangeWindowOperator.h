#pragma once
#include "../Common/stdafx.h"
#include "../Operator/Operator.h"
#include "../Internal/Synopsis/WindowSynopsis.h"
#include "../Internal/Queue/QueueEntity.h"
#include <boost/shared_ptr.hpp>
class RangeWindowOperator:public Operator
{
private:
	const Timeunit rangeWindowSize;

	boost::shared_ptr<WindowSynopsis> windowSynopsis;
	Timestamp lastInputTimestamp;
	Timestamp oldestElementTimestampInWinSynopsis;

	//void expireElement(Timestamp timestamp, Element& inputElement);
	void expireElement(Element& inputElement);

public:
	RangeWindowOperator(Timeunit timeRange);
	~RangeWindowOperator(void);

	Timeunit getWindowSize();

	void execution();
};


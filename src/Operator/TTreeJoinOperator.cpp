/*
 * TTreeTTreeJoinOperator.cpp
 *
 *  Created on: Oct 19, 2015
 *      Author: root
 */

#include "TTreeJoinOperator.h"
#include "../Common/stdafx.h"
#include "../Operator/JoinOperator.h"
#include "../Internal/Synopsis/LineageSynopsis.h"
#include "../Internal/Queue/QueueEntity.h"
#include "../Utility/DocumentIdentifierGenerator.h"
#include "../Internal/Synopsis/RelationSynopsis.h"
#include "../Internal/Element/ElementIterator.h"
#include "../Scheduler/Scheduler.h"
#include "../Utility/TimeCounterUtility.h"
#include "../Query/QueryUtility.h"

#include "../Utility/DocumentIdentifierGenerator.h"
#include "../Utility/TimestampGenerator.h"
#include "../Utility/TimeCounterUtility.h"
#include "../BinaryJson/json.h"

#include "../IO/DBManager.h"
using namespace oracle::occi;

namespace ttree {

TTreeJoinOperator::TTreeJoinOperator() {

	int lineageNumber = 2;
	this->lineageSynopsis.reset(new LineageSynopsis(lineageNumber));
	this->leftRelationSynopsis.reset(new RelationSynopsis());

	initialTTreeSynopsis();

	//	this->lastLeftTimestamp = 0;
	//	this->lastRightTimestamp = 0;
	this->leftElementIterator.reset(
			new ElementIterator(this->leftRelationSynopsis));

	this->leftOuter = false;
	this->rightOuter = false;

}

void TTreeJoinOperator::initialTTreeSynopsis() {

	this->rightTTreeSynopsis.reset(new TTreeSynopsis());

	rightTTreeSynopsis->buffer->indexName = "PK";
	//rightTTreeSynopsis->secondBuffers.at(0)->indexName = this->rightJoinAttribute->getLastAttribute();
	rightTTreeSynopsis->secondBuffers.at(0)->indexName = "A";

}

void TTreeJoinOperator::setLeftJoinAttribute(
		boost::shared_ptr<QueryAttribute> leftJoinAttribute) {
	this->leftJoinAttribute = leftJoinAttribute;
}
void TTreeJoinOperator::setRightJoinAttribute(
		boost::shared_ptr<QueryAttribute> rightJoinAttribute) {
	this->rightJoinAttribute = rightJoinAttribute;
}
void TTreeJoinOperator::setLeftOuter(bool bl) {
	this->leftOuter = bl;
}
void TTreeJoinOperator::setRightOuter(bool bl) {
	this->rightOuter = bl;
}
void TTreeJoinOperator::setResultQueryProjection(
		boost::shared_ptr<QueryProjectionAbstract> resultQueryProjection) {
	this->resultQueryProjection = resultQueryProjection;
}
TTreeJoinOperator::~TTreeJoinOperator() {
	// TODO Auto-generated destructor stub
}
void TTreeJoinOperator::execution() {
#ifdef DEBUG
	std::cout<<"===================operator begin================="<<std::endl;
	std::cout<<"operatorid : "<<this->getId()<<std::endl;
#endif
	assert(this->getInputQueueList().size() == 2);
	assert(this->getOutputQueueList().size() == 1);
	boost::shared_ptr<QueueEntity> leftInputQueue =
			this->getInputQueueList().front();
	boost::shared_ptr<QueueEntity> rightInputQueue =
			this->getInputQueueList().back();
	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();

	long long minimumLeftQueueLastTimestamp = -1;
	long long minimumRightQueueLastTimestamp = -1;
	//minimumLeftQueueLastTimestamp = lastLeftTimestamp;
	//minimumRightQueueLastTimestamp = lastRightTimestamp;

	while (1) {
		Element leftElement;
		Element rightElement;
		if (outputQueue->isFull()) {
			break;
		}
		if (!leftInputQueue->isEmpty()) {
			leftInputQueue->peek(leftElement);
			minimumLeftQueueLastTimestamp = leftElement.timestamp;
		}
		if (!rightInputQueue->isEmpty()) {
			rightInputQueue->peek(rightElement);
			minimumRightQueueLastTimestamp = rightElement.timestamp;
		}
		if (leftElement.timestamp == -1 && rightElement.timestamp == -1) {
			break;
		}

		if ((!leftInputQueue->isEmpty()) && (!rightInputQueue->isEmpty())
				&& minimumLeftQueueLastTimestamp
						< minimumRightQueueLastTimestamp) {
			assert(!leftInputQueue->isEmpty());

			TimeCounterUtility::getInstance()->start();
			leftInputQueue->dequeue();
			if (leftElement.mark == PLUS_MARK) {
				//processLeftPlus(leftElement);
				processLeftPlusDirect(leftElement);
			} else if (leftElement.mark == MINUS_MARK) {
				//processLeftMinus(leftElement);
				processLeftMinusDirect(leftElement);
			}
			TimeCounterUtility::getInstance()->pause();
		} else if ((!leftInputQueue->isEmpty()) && (!rightInputQueue->isEmpty())
				&& minimumLeftQueueLastTimestamp
						> minimumRightQueueLastTimestamp) {
			assert(!rightInputQueue->isEmpty());

			TimeCounterUtility::getInstance()->start();
			rightInputQueue->dequeue();
			if (rightElement.mark == PLUS_MARK) {
				processRightPlus(rightElement);
			} else if (rightElement.mark == MINUS_MARK) {
				processRightMinus(rightElement);
			}
			TimeCounterUtility::getInstance()->pause();
		} else if (!leftInputQueue->isEmpty()) {
			TimeCounterUtility::getInstance()->start();
			leftInputQueue->dequeue();
			if (leftElement.mark == PLUS_MARK) {
				//processLeftPlus(leftElement);
				processLeftPlusDirect(leftElement);
			} else if (leftElement.mark == MINUS_MARK) {
				//processLeftMinus(leftElement);
				processLeftMinusDirect(leftElement);
			}
			TimeCounterUtility::getInstance()->pause();
		} else if (!rightInputQueue->isEmpty()) {
			TimeCounterUtility::getInstance()->start();
			rightInputQueue->dequeue();
			if (rightElement.mark == PLUS_MARK) {
				processRightPlus(rightElement);
			} else if (rightElement.mark == MINUS_MARK) {
				processRightMinus(rightElement);
			}
			TimeCounterUtility::getInstance()->pause();
		}
	}
#ifdef DEBUG
	std::cout<<"===================operator over================="<<std::endl;
#endif
}
void TTreeJoinOperator::processLeftPlus(Element& leftElement) {

	std::vector<Element> elements;

	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();
	Document& leftDocument = leftElement.document;
	if (this->leftRelationSynopsis->isFull()) {
		throw runtime_error("left synopsis is full");
	}

	this->leftRelationSynopsis->insertElement(leftElement);

	int leftValue = (boost::any_cast<int>)(
			this->leftJoinAttribute->getValue(leftDocument));

	this->rightTTreeSynopsis->findElementListByKeyIn2ndBuffer(leftValue,
			elements);

	vector<Element>::iterator it;
	for (it = elements.begin(); it != elements.end(); it++) {
		Document rightDocument = it->document;

		boost::any retDocument = this->resultQueryProjection->performProjection(
				leftDocument, rightDocument);
		assert(retDocument.type() == typeid(Document));
		Document newDocument = boost::any_cast<Document>(retDocument);

		Element newElement;
		newElement.id =
				DocumentIdentifierGenerator::generateNewDocumentIdentifier();
		newElement.document = newDocument;
		newElement.mark = PLUS_MARK;
		newElement.timestamp = leftElement.timestamp;
		newElement.masterTag = leftElement.masterTag;
		//insert into lineage synopsis
		if (this->lineageSynopsis->isFull()) {
			throw runtime_error("lineage synopsis is full");
		}
		Lineage lineage;
		lineage.lineageNumber = 2;
		lineage.lineageDocumentId[0] = leftElement.id;
		lineage.lineageDocumentId[1] = it->id;
		this->lineageSynopsis->insertLineage(lineage, newElement);

		//generate new output to the output queue
		if (outputQueue->isFull()) {
			throw std::runtime_error("output queue is full");
		}
		outputQueue->enqueue(newElement);

	}

	assert(!outputQueue->isFull());

}
void TTreeJoinOperator::processLeftMinus(Element& leftElement) {

	std::vector<Element> elements;

	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();
	Document& leftDocument = leftElement.document;
	this->leftRelationSynopsis->deleteElement(leftElement);

	int leftValue = (boost::any_cast<int>)(
			this->leftJoinAttribute->getValue(leftDocument));
	this->rightTTreeSynopsis->findElementListByKeyIn2ndBuffer(leftValue,
			elements);
	vector<Element>::iterator it;
	for (it = elements.begin(); it != elements.end(); it++) {

		Lineage lineage;
		lineage.lineageNumber = 2;
		lineage.lineageDocumentId[0] = leftElement.id;
		lineage.lineageDocumentId[1] = it->id;
		Element elementInSynopsis;
		this->lineageSynopsis->getAndDeleteElement(lineage, elementInSynopsis);

		//generate output element
		Element outputElement;
		outputElement.mark = MINUS_MARK;
		outputElement.id = elementInSynopsis.id;
		outputElement.timestamp = leftElement.timestamp;
		outputElement.document = elementInSynopsis.document;
		outputElement.masterTag = leftElement.masterTag;
		//output element
		if (outputQueue->isFull()) {
			throw std::runtime_error("output queue is full");
		}

		outputQueue->enqueue(outputElement);
	}

	assert(!outputQueue->isFull());
}
void TTreeJoinOperator::processRightPlus(Element& rightElement) {

	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();
	Document& rightDocument = rightElement.document;
	if (this->rightTTreeSynopsis->isFull()) {
		throw runtime_error("left synopsis is full");
	}
	this->rightTTreeSynopsis->insertElement(rightElement);

	this->leftElementIterator->initial();
	Element leftElement;
	while ((!outputQueue->isFull())
			&& this->leftElementIterator->getNext(leftElement)) {

		Document leftDocument = leftElement.document;

		boost::any leftValue = this->leftJoinAttribute->getValue(leftDocument);
		boost::any rightValue = this->rightJoinAttribute->getValue(
				rightDocument);
		//	//wangyan
		if (!QueryUtility::compareEqual(leftValue, rightValue)) {
			continue;
		}
		boost::any retDocument = this->resultQueryProjection->performProjection(
				leftDocument, rightDocument);
		assert(retDocument.type() == typeid(Document));
		Document newDocument = boost::any_cast<Document>(retDocument);
		Element newElement;
		newElement.id =
				DocumentIdentifierGenerator::generateNewDocumentIdentifier();
		newElement.document = newDocument;
		newElement.mark = PLUS_MARK;
		newElement.timestamp = rightElement.timestamp;
		newElement.masterTag = rightElement.masterTag;
		//insert into lineage synopsis
		if (this->lineageSynopsis->isFull()) {
			throw runtime_error("lineage synopsis is full");
		}
		Lineage lineage;
		lineage.lineageNumber = 2;
		lineage.lineageDocumentId[0] = leftElement.id;
		lineage.lineageDocumentId[1] = rightElement.id;

		cout << "plus: " << leftElement.id << ": " << rightElement.id << endl;

		this->lineageSynopsis->insertLineage(lineage, newElement);

		//generate new output to the output queue
		if (outputQueue->isFull()) {
			throw std::runtime_error("output queue is full");
		}
		outputQueue->enqueue(newElement);

	}

	assert(!outputQueue->isFull());
}
void TTreeJoinOperator::processRightMinus(Element& rightElement) {

	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();
	Document& rightDocument = rightElement.document;

	Element tmpElement;
	tmpElement.document = rightDocument.copy();
	this->rightTTreeSynopsis->deleteElement(tmpElement);

	this->leftElementIterator->initial();
	Element leftElement;

	while ((!outputQueue->isFull())
			&& this->leftElementIterator->getNext(leftElement)) {


		Document& leftDocument = leftElement.document;
		boost::any leftValue = this->leftJoinAttribute->getValue(leftDocument);
		boost::any rightValue = this->rightJoinAttribute->getValue(
				rightDocument);
		//	//wangyan
		if (!QueryUtility::compareEqual(leftValue, rightValue)) {
			continue;
		}
		Lineage lineage;
		lineage.lineageNumber = 2;
		lineage.lineageDocumentId[0] = leftElement.id;
		lineage.lineageDocumentId[1] = rightElement.id;


		Element elementInSynopsis;
		if (!this->lineageSynopsis->getAndDeleteElement(lineage,
				elementInSynopsis)) {
			break;
		}
		//generate output element
		Element outputElement;
		outputElement.mark = MINUS_MARK;
		outputElement.id = elementInSynopsis.id;
		outputElement.timestamp = rightElement.timestamp;
		outputElement.document = elementInSynopsis.document;
		outputElement.masterTag = rightElement.masterTag;
		//output element
		if (outputQueue->isFull()) {
			throw std::runtime_error("output queue is full");
		}
		outputQueue->enqueue(outputElement);

	}
	assert(!outputQueue->isFull());

}

void TTreeJoinOperator::processLeftPlusDirect(Element& leftElement) {

	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();
	Document& leftDocument = leftElement.document;
	if (this->leftRelationSynopsis->isFull()) {
		throw runtime_error("left synopsis is full");
	}
	this->leftRelationSynopsis->insertElement(leftElement);
	//cout << "leftPlusDocument: " << leftDocument << endl;
	Element rightElement;

	//todo for test...

	double leftValue = boost::any_cast<int>(
			this->leftJoinAttribute->getValue(leftDocument));
	string sqlStmt = "select a, b, pk from test where a = "
			+ boost::lexical_cast<string>(leftValue);
	Statement *stmt =
			DBManager::getInstance()->getDBConnection()->createStatement(
					sqlStmt);
	ResultSet *rset = stmt->executeQuery();
	while (rset->next()) {

		//right document
		DocumentBuilder documentBuilder;
		documentBuilder.append("A", rset->getInt(1));
		documentBuilder.append("B", rset->getInt(2));
		documentBuilder.append("PK", rset->getInt(3));

		Document tmpTightDocument = documentBuilder.obj();
		Document& rightDocument = tmpTightDocument;

		boost::any retDocument = this->resultQueryProjection->performProjection(
				leftDocument, rightDocument);
		assert(retDocument.type() == typeid(Document));
		Document newDocument = boost::any_cast<Document>(retDocument);
		Element newElement;
		newElement.id =
				DocumentIdentifierGenerator::generateNewDocumentIdentifier();
		newElement.document = newDocument;
		newElement.mark = PLUS_MARK;
		newElement.timestamp = leftElement.timestamp;
		newElement.masterTag = leftElement.masterTag;
		//insert into lineage synopsis
		if (this->lineageSynopsis->isFull()) {
			throw runtime_error("lineage synopsis is full");
		}
		Lineage lineage;
		lineage.lineageNumber = 2;
		lineage.lineageDocumentId[0] = leftElement.id;
		lineage.lineageDocumentId[1] = rightElement.id;
		this->lineageSynopsis->insertLineage(lineage, newElement);

		//generate new output to the output queue
		if (outputQueue->isFull()) {
			throw std::runtime_error("output queue is full");
		}
		outputQueue->enqueue(newElement);

	}
	DBManager::getInstance()->getDBConnection()->terminateStatement(stmt);
	assert(!outputQueue->isFull());

}
void TTreeJoinOperator::processLeftMinusDirect(Element& leftElement) {
	boost::shared_ptr<QueueEntity> outputQueue =
			this->getOutputQueueList().front();
	Document& leftDocument = leftElement.document;
	this->leftRelationSynopsis->deleteElement(leftElement);
	//cout << "leftMinusDocument: " << leftDocument << endl;

	Element rightElement;
	//test

	//todo for test...
	double leftValue = boost::any_cast<int>(
			this->leftJoinAttribute->getValue(leftDocument));
	string sqlStmt = "select a, b, pk from test where a = "
			+ boost::lexical_cast<string>(leftValue);
	Statement *stmt =
			DBManager::getInstance()->getDBConnection()->createStatement(
					sqlStmt);
	ResultSet *rset = stmt->executeQuery();
	while (rset->next()) {

		//right document
		DocumentBuilder documentBuilder;
		documentBuilder.append("A", rset->getInt(1));
		documentBuilder.append("B", rset->getInt(2));
		documentBuilder.append("PK", rset->getInt(3));

		Document tmpRightDocument = documentBuilder.obj();
		Document& rightDocument = tmpRightDocument;

		//cout << "left-rightDocument: " << rightDocument << endl;

		Lineage lineage;
		lineage.lineageNumber = 2;
		lineage.lineageDocumentId[0] = leftElement.id;
		lineage.lineageDocumentId[1] = rightElement.id;
		Element elementInSynopsis;
		this->lineageSynopsis->getAndDeleteElement(lineage, elementInSynopsis);

		//generate output element
		Element outputElement;
		outputElement.mark = MINUS_MARK;
		outputElement.id = elementInSynopsis.id;
		outputElement.timestamp = leftElement.timestamp;
		outputElement.document = elementInSynopsis.document;
		outputElement.masterTag = leftElement.masterTag;
		//output element
		if (outputQueue->isFull()) {
			throw std::runtime_error("output queue is full");
		}
		outputQueue->enqueue(outputElement);

	}
	DBManager::getInstance()->getDBConnection()->terminateStatement(stmt);

	assert(!outputQueue->isFull());
}

} /* namespace ttree */

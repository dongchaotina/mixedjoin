#pragma once
#include "../Common/stdafx.h"
#include "../IO/IStreamOutput.h"
class SocketStreamOutput :
	public IStreamOutput
{
private:
	
public:
	std::string ip;
	std::string port;
	SocketStreamOutput(std::string ip,std::string port);
	~SocketStreamOutput(void);
	void pushNext(Element& element) ;
	bool isFull(void) ;
	void initial(void) ;
};


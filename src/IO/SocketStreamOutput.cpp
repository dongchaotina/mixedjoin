#include "../Common/stdafx.h"
#include "../IO/SocketStreamOutput.h"
#include "../IO/IOManager.h"
#include "../Internal/Buffer/BufferConstraint.h"
static char socketbuffer[CHUNK_SIZE] ;
SocketStreamOutput::SocketStreamOutput(std::string ip,std::string port)
{
	this->ip = ip;
	this->port = port;
}


SocketStreamOutput::~SocketStreamOutput(void)
{
}
void SocketStreamOutput::pushNext(Element& element) 
{
//	std::cout<<"________system output start__________"<<std::endl;
	int size = element.getSize();
	
	int p = 0;
	*(Timestamp*)(socketbuffer + p) = element.timestamp;
	//memcpy(socketbuffer + p , &element.timestamp,TIMESTAMP_SIZE);
	p += TIMESTAMP_SIZE;
	*(DocumentId*)(socketbuffer + p) = element.id;
	//memcpy(socketbuffer + p , &element.id, DOCUMENT_IDENTIFIER_SIZE);
	p += DOCUMENT_IDENTIFIER_SIZE;
	*(Mark*)(socketbuffer + p) = element.mark;
	//memcpy(socketbuffer + p , &element.mark, MARK_SIZE);
	p += MARK_SIZE;

	*(MasterTag*)(socketbuffer + p) = element.masterTag;
	p += MASTER_TAG_SIZE;

	memcpy(socketbuffer + p , element.document.objdata(),element.document.objsize());
	p += element.document.objsize();

	IOManager::getInstance()->writeOutput(socketbuffer,size,this);
	
	//std::cout<<element<<std::endl;
	//std::cout<<"________system output finish_________"<<std::endl;
}
bool SocketStreamOutput::isFull(void) 
{
	return false;
}
void SocketStreamOutput::initial(void)
{
	IOManager::getInstance()->addStreamOutput(ip,port,this);
}
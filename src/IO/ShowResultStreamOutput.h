#pragma once
#include "../Common/stdafx.h"
#include "../IO/IStreamOutput.h"
class ShowResultStreamOutput : // show the query output element to the standard output by cout<<
	public IStreamOutput
{
public:
	ShowResultStreamOutput(void);
	~ShowResultStreamOutput(void);
	void pushNext(Element& element);
	bool isFull(void) ;
	void initial(void) ;
};


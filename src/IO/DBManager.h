/*
 * DBManager.h
 *
 *  Created on: Jun 20, 2015
 *      Author: root
 */

#ifndef SRC_IO_DBMANAGER_H_
#define SRC_IO_DBMANAGER_H_

#include <occi.h>
using namespace oracle::occi;

using namespace std;
using namespace oracle::occi;

class DBManager {
private:
	Environment *env;
	Connection *conn;
	Statement *stmt;

	Statement *stmtInsert;
	Statement *stmtSelect;
	Statement *stmtDelete;

	static DBManager * dbManager;
	DBManager();
	void initial(void);
public:
	static DBManager* getInstance(void);

	virtual ~DBManager();
	Connection *getDBConnection();
	Statement *getStmtInsert();
	Statement *getStmtSelect();
	Statement *getStmtDelete();

};

#endif /* SRC_IO_DBMANAGER_H_ */

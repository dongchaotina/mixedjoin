#include "../Common/stdafx.h"
#include "../IO/SocketStreamInput.h"
#include "../IO/IStreamInput.h"
#include "../Internal/Element/Element.h"
#include "../IO/IOManager.h"
#include "../Server/JsonStreamServer.h"
#include "../Schema/JsonSchema.h"
#include "../Utility/TimeCounterUtility.h"
#include "../Utility/TimestampGenerator.h"
#include "../Utility/DocumentIdentifierGenerator.h"
#include <boost/shared_ptr.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include "../Internal/Synopsis/WindowSynopsis.h"
int SocketStreamInput::ignoredInputNumber = 0;
SocketStreamInput::SocketStreamInput(std::string ip,std::string port,boost::shared_ptr<JsonSchema> schema):IStreamInput(schema)
{
	this->ip = ip;
	this->port = port;
	this->windowSynopsis.reset(new WindowSynopsis()); 
}


SocketStreamInput::~SocketStreamInput(void)
{
}
bool SocketStreamInput::checkWindowSynopsisCorrect(int number)
{
	std::list<Element> elementList;
	boost::mutex::scoped_lock lock(m_oMutex);
	bool bl = this->windowSynopsis->isEmpty();
	if(bl==true)
	{
		return true;
	}
	this->windowSynopsis->peekAllElements(elementList);
	std::list<Element>::iterator it;
	for(it = elementList.begin();it!=elementList.end();it++)
	{
		Element element = *it;
		if(element.document.hasField("connectionNumber")&&this->getSchema()->getId()=="log_stream")
		{
			std::cout<<"error "<<number<<std::endl;
			exit(1);
			return false;
		}
		if(element.document.hasField("queryString")&&this->getSchema()->getId()=="connection_stream")
		{
			std::cout<<"error "<<number<<std::endl;
			exit(1);
			return false;
		}
	}
	lock.unlock();
	return true;
}	
void SocketStreamInput::getNextElement(Element& element)
{
	//checkWindowSynopsisCorrect(1);
	boost::mutex::scoped_lock lock(m_oMutex);
	//std::cout<<this->windowSynopsis->getElementNumber()<<std::endl;
	this->windowSynopsis->getOldestElement(element);
	element.id = DocumentIdentifierGenerator::generateNewDocumentIdentifier();
	element.mark = PLUS_MARK;
	element.timestamp = TimestampGenerator::getCurrentTime();
	element.document.getOwned();
	this->windowSynopsis->deleteOldestElement();
	lock.unlock();
	//checkWindowSynopsisCorrect(2);
}
bool SocketStreamInput::isEmpty()
{
	boost::mutex::scoped_lock lock(m_oMutex);
	bool bl = this->windowSynopsis->isEmpty();
	lock.unlock();
	return bl;
}
char stream_buffer[4096];
int temp = 0 ;
void SocketStreamInput::readData(char* data, int length)
{
	//checkWindowSynopsisCorrect(3);
	boost::mutex::scoped_lock lock(m_oMutex);
	boost::asio::streambuf& streambuffer = this->streambuffer;
	std::ostream os(&streambuffer);
	os.write(data,length);
	std::istream is (&streambuffer);
	while(this->streambuffer.size()>0)
	{
		if(this->streambuffer.size()<TIMESTAMP_SIZE + DOCUMENT_IDENTIFIER_SIZE + MARK_SIZE + MASTER_TAG_SIZE + sizeof(int))
		{
			break;
		}
		
	
		Element element;
		
		int p = 0;
		is.read(stream_buffer + p,TIMESTAMP_SIZE);
		element.timestamp = *(Timestamp*)(stream_buffer+p);
		p += TIMESTAMP_SIZE;

		is.read(stream_buffer + p,DOCUMENT_IDENTIFIER_SIZE);
		element.id = *(DocumentId*)(stream_buffer+p);
		p += DOCUMENT_IDENTIFIER_SIZE;

		is.read(stream_buffer + p,MARK_SIZE);
		element.mark = *(Mark*)(stream_buffer + p);
		p += MARK_SIZE;

		is.read(stream_buffer + p, MASTER_TAG_SIZE);
		element.masterTag = *(MasterTag*)(stream_buffer + p);
		p += MASTER_TAG_SIZE;

		is.read(stream_buffer + p, sizeof(int)); //document size;
		int documentSize = *(int*)(stream_buffer + p);
		p += sizeof(int);

		if(this->streambuffer.size()<documentSize)
		{
			int remainBytes = streambuffer.size(); 
			is.read(stream_buffer + p ,remainBytes );
			p += remainBytes;
			os.write(stream_buffer, p);
			break;
		}
		else
		{
			
			char* documentPosition = stream_buffer + p - sizeof(int);
			is.read(stream_buffer + p, documentSize - sizeof(int));


			Document document (documentPosition);	
			element.document = document;
			element.document.getOwned();
			
			// load shedding
			if(this->windowSynopsis->getElementNumber()>100)
			{
				ignoredInputNumber ++ ;
				//std::cout<<this->windowSynopsis->getElementNumber()<<std::endl;
				break;
			}


			if(this->windowSynopsis->isFull()==true)
			{
				std::cout<<"synopsis out of memory error"<<std::endl;
				exit(1);
			}
			else
			{//not full 
				this->windowSynopsis->insertElement(element);
			}

		}

		
		
	}
	lock.unlock();
	//checkWindowSynopsisCorrect(4);
	
	
}
void SocketStreamInput::initial()
{
	IOManager::getInstance()->addStreamInput(ip,port,this);
}


#pragma once
#include "../Common/stdafx.h"
#include "../Internal/Element/Element.h"
#include "../IO/IStreamInput.h"
#include "../Schema/JsonSchema.h"
class RandomGeneratedStreamInput: public IStreamInput
{
private:
	boost::shared_ptr<JsonSchema> schema;
	void getNextElement(Element& element);
	Document bufferedDocument;	
public:
	RandomGeneratedStreamInput(boost::shared_ptr<JsonSchema> schema);
	~RandomGeneratedStreamInput(void);
	bool isEmpty();
	void initial();
	static int throughout;
};


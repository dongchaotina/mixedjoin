#include "../Common/stdafx.h"
#include "../IO/SpecifiedInputRateStreamInput.h"
#include "../Schema/SchemaInterpreter.h"
#include "../Utility/DocumentIdentifierGenerator.h"
#include "../Utility/TimestampGenerator.h"
#include "../Utility/SleepUtility.h"
#include <boost/thread.hpp>
#include <boost/bind.hpp>  
#include <boost/function.hpp>  
int SpecifiedInputRateStreamInput::ignoredInputNumber = 0;
SpecifiedInputRateStreamInput::SpecifiedInputRateStreamInput(
		boost::shared_ptr<JsonSchema> schema) :
		IStreamInput(schema) {
	this->schema = schema;
	SchemaInterpreter::generateRandomDocumentBySchema(this->schema,
			this->bufferedDocument);
	this->bufferedElementNumber = 0;
	this->totalElementNumber = 0;
}

SpecifiedInputRateStreamInput::~SpecifiedInputRateStreamInput(void) {
	std::cout << this->getId() << " total element number : "
			<< totalElementNumber << std::endl;
}
void SpecifiedInputRateStreamInput::getNextElement(Element& element) {
	boost::recursive_mutex::scoped_lock lock(m_oMutex);
	this->bufferedElementNumber--;
	assert(this->bufferedElementNumber >= 0);
	lock.unlock();
	element.id = DocumentIdentifierGenerator::generateNewDocumentIdentifier();
	element.mark = PLUS_MARK;
	element.timestamp = TimestampGenerator::getCurrentTime();

	SchemaInterpreter::generateRandomDocumentBySchema(this->schema,
			this->bufferedDocument);

	element.document = this->bufferedDocument;
	element.document.getOwned();
}
bool SpecifiedInputRateStreamInput::isEmpty() {
	boost::recursive_mutex::scoped_lock lock(m_oMutex);
	bool bl = false;
	if (bufferedElementNumber == 0) {
		bl = true;
	}
	lock.unlock();
	return bl;
}
void SpecifiedInputRateStreamInput::initial() {
	int sleepMiliSeconds = 20;
	//std::cout<<"creating one thread+++++++++++++++"<<std::endl;
	boost::function<void(int)> memberFunctionWrapper(
			boost::bind(&SpecifiedInputRateStreamInput::generateInput, this,
					_1));
	boost::thread downloadThread(
			(boost::bind(memberFunctionWrapper, sleepMiliSeconds)));
}
void SpecifiedInputRateStreamInput::generateInput(int sleepMiliSeconds) {
	while (1) {

		if (this->getId() == "performanceTestStream1") {
			boost::recursive_mutex::scoped_lock lock(m_oMutex);
			this->bufferedElementNumber++;

			lock.unlock();
			this->totalElementNumber++;
			SleepUtility::sleepMicroSecond(1000); //1000tuple/s
		} else if (this->getId() == "performanceTestStream2") {

			boost::recursive_mutex::scoped_lock lock(m_oMutex);
			this->bufferedElementNumber++;

			lock.unlock();
			this->totalElementNumber++;
			SleepUtility::sleepMicroSecond(100); //tuple/s
		}
	}
}

/*
 * DBManager.cpp
 *
 *  Created on: Jun 20, 2015
 *      Author: root
 */

#include "DBManager.h"
DBManager * DBManager::dbManager = NULL;
DBManager::DBManager() {
	initial();
}

DBManager::~DBManager() {
}

DBManager* DBManager::getInstance(void) {
	if (dbManager == NULL) {
		dbManager = new DBManager();
	}
	return dbManager;
}
void DBManager::initial() {
	env = Environment::createEnvironment(Environment::DEFAULT);
	conn = env->createConnection("dong", "moonone", "XE");

	string sqlInsert = "insert into WINDOW_TEMP (A, B) values (:1, :2)";
	stmtInsert = conn->createStatement(sqlInsert);
	string sqlSelect =
			"select t1.a, t1.b, t2.a from test t1, window_temp t2 where t1.a = t2.a";

	stmtSelect = conn->createStatement(sqlSelect);
	string sqlDelete = "delete from window_temp";
	stmtDelete = conn->createStatement(sqlDelete);

}

Connection *DBManager::getDBConnection() {
	return this->conn;
}

Statement *DBManager::getStmtInsert() {
	return this->stmtInsert;
}
Statement *DBManager::getStmtSelect() {
	return this->stmtSelect;
}
Statement *DBManager::getStmtDelete() {
	return this->stmtDelete;
}

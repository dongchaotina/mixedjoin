#pragma once
#include "../Common/stdafx.h"
#include "../Internal/Element/Element.h"
#include "../IO/IStreamInput.h"
#include "../Schema/JsonSchema.h"
#include <boost/asio.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include "../Internal/Synopsis/WindowSynopsis.h"
class SocketStreamInput:public IStreamInput
{
private:
	boost::asio::streambuf streambuffer;
	std::string ip;
	std::string port;
	void getNextElement(Element& element);
	boost::mutex m_oMutex;
	boost::shared_ptr<WindowSynopsis> windowSynopsis; 
	bool checkWindowSynopsisCorrect(int number);
public:
	SocketStreamInput(std::string ip,std::string port,boost::shared_ptr<JsonSchema> schema);
	~SocketStreamInput(void);
	void readData(char* data, int length);
	bool isEmpty();
	void initial();
	static int ignoredInputNumber;
};


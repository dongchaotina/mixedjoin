#pragma once
#include "../Common/stdafx.h"
#include "../IO/IStreamInput.h"
class PeopleFlowStreamInput :
	public IStreamInput
{
private:
	std::vector<std::string> dataFolderVector;// the folders containing the people flow data
	std::list<std::string> peopleFlowFileList;// the list containing all the file names in all of the folders
	std::list<std::string>::iterator currentPeopleFlowFileListIterator; // the iterator used to scan the file list
	std::ifstream currentFileStream; // the current file 
	void getNextElement(Element& element) ;
public:
	PeopleFlowStreamInput(std::vector<std::string> dataFolderVector,boost::shared_ptr<JsonSchema>jsonSchema);
	~PeopleFlowStreamInput(void);
	bool isEmpty() ;
	void initial() ;
};


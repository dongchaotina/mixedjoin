#pragma once
#include "../Common/stdafx.h"
#include "../Internal/Element/Element.h"
#include "../Schema/JsonSchema.h"
#include <boost/asio.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/shared_ptr.hpp>
/*
 * Any wrapper of information source should implement this interface 
 */
class IStreamInput
{
private:
	bool isActive;
	std::string id;
	boost::shared_ptr<JsonSchema>jsonSchema;
	virtual void getNextElement(Element& element) = 0;
	virtual void initial() = 0;
public:

	IStreamInput(boost::shared_ptr<JsonSchema>jsonSchema);
	virtual ~IStreamInput(void);
	bool getAndCheckNextElement(Element& element);
	virtual bool isEmpty() = 0;
	void start();
	std::string getId(void);
	boost::shared_ptr<JsonSchema>getSchema(void);
};


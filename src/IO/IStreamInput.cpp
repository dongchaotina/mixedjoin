#include "../Common/stdafx.h"
#include "../IO/IStreamInput.h"
#include "../Internal/Element/Element.h"
#include "../IO/IOManager.h"
#include "../Schema/SchemaInterpreter.h"
IStreamInput::IStreamInput(boost::shared_ptr<JsonSchema>jsonSchema)
{
	this->jsonSchema = jsonSchema;
	this->id = jsonSchema->getId();
	this->isActive = false;
}


IStreamInput::~IStreamInput(void)
{
}
std::string IStreamInput::getId(void)
{
	return this->id;
}
bool IStreamInput::getAndCheckNextElement(Element& element)
{
	bool bl = true;
	getNextElement(element);
#ifdef CHECKINPUTSCHEMA
	bl = SchemaInterpreter::checkDocumentSatisfiedSchema(element.document,this->jsonSchema);
#endif

	return bl;
}
boost::shared_ptr<JsonSchema> IStreamInput::getSchema(void)
{
	return this->jsonSchema;
}

void IStreamInput::start()
{
	if(this->isActive == false)
	{
		initial();
		this->isActive = true;
	}
}

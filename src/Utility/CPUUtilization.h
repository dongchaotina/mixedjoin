
class CPUUtilization
{
    public:
        CPUUtilization();
        ~CPUUtilization();

        void init();
        double getCurrentValue();

    protected:
    private:
        static unsigned long long lastTotalUser, lastTotalUserLow, lastTotalSys, lastTotalIdle;

};

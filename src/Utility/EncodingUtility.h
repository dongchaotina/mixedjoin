#pragma once
#include "../Common/stdafx.h"
class EncodingUtility
{
private:
	static std::string currentSystemStringEncoding;//unicode
public:
	EncodingUtility(void);
	~EncodingUtility(void);
	static std::string convertEncoding(std::string const& text, std::string const& to_encoding, std::string const& from_encoding);
	
	static std::string getCurrentSystemStringEncoding();
};


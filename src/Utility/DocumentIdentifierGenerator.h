#pragma once
#include "../Common/stdafx.h"
#include "../Common/Types.h"
#include <boost/noncopyable.hpp>
/*
 generate identifier for document
 currently the id is an incrementally added integer
*/
class DocumentIdentifierGenerator
{
private:
	static DocumentId identifier;
	
	DocumentIdentifierGenerator(void);
	~DocumentIdentifierGenerator(void);

public:

	static DocumentId generateNewDocumentIdentifier(void);
	
};


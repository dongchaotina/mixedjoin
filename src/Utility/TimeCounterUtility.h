#pragma once
#include "../Common/stdafx.h"
#include <boost/noncopyable.hpp>
enum TimeCounterStatus
{
	initialStatus,
	runningStatus,
	pauseStatus,
};
class TimeCounterUtility:
	public boost::noncopyable
{
private:
	static TimeCounterUtility* timeCounterUtility;
	TimeCounterUtility(void);
#ifdef WINDOWS
	double totalTime;
	 double dff;  
	 long long startTime ;
#else
	double totalTime;
	struct timeval startTime;	
#endif
	 TimeCounterStatus timeCounterStatus;
public:
	~TimeCounterUtility(void);
	static TimeCounterUtility* getInstance();
	 void reset();
	 void start();
	 void pause();
	 void stop(double& time);
};


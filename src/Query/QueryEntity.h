#pragma once
#include <boost/shared_ptr.hpp>
#include "../Query/QueryIntermediateRepresentation.h"
#include "../IO/IStreamOutput.h"
#include "../Operator/LeafOperator.h"
#include "../Operator/Operator.h"
#include "../Common/Types.h"
#include <boost/tuple/tuple.hpp>
#include "../Internal/Queue/QueueEntity.h"
class RootOperator;
class QueryEntity
{
private:
	std::string queryId;
	boost::shared_ptr<IStreamOutput> streamOutput;
//	std::list<boost::shared_ptr<Operator> >operatorList;
//	bool active;
	TimeDuration activateDuration;
	TimeDuration rangeWindowSize;
	Timestamp lastActivatedTimestamp;
	std::map<LeafOperator*, bool> masterTagMap;
	std::map<LeafOperator*, boost::shared_ptr<QueueEntity> > outputQueueMap;
	boost::shared_ptr<RootOperator> rootOperator;
public:
	QueryEntity(boost::shared_ptr<QueryIntermediateRepresentation>queryIntermediateRepresentation,boost::shared_ptr<IStreamOutput> streamOutput, std::string durationSpecification);
	~QueryEntity();
	void setActive(Timestamp ts);
	bool isActive(Timestamp ts);
	void addMasterTag(LeafOperator* leafOperator, bool masterTag);
	bool getMasterTag(LeafOperator* leafOperator);
	void setRangeWindowSize(TimeDuration rangeWindowSize);
	TimeDuration getRangeWindowSize();

	void addOutputQueue(LeafOperator* leafOperator, boost::shared_ptr<QueueEntity> outputQueue);
	boost::shared_ptr<QueueEntity> getOutputQueue (LeafOperator* leafOperator) ;
	friend class QueryIntermediateRepresentationInterpreter;
	void changeLeafOperator(boost::shared_ptr<LeafOperator>fromOperator, boost::shared_ptr<LeafOperator>toOperator);
	Timestamp getLastActivatedTimestamp();
	boost::shared_ptr<RootOperator> getRootOperator();
};

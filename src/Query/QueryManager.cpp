#include "../Common/stdafx.h"
#include "../Query/QueryManager.h"
#include <boost/foreach.hpp>
#include <boost/unordered_map.hpp>  
#include <map>
#include "QueryEntity.h"
QueryManager*  QueryManager::queryManager = NULL;
QueryManager::QueryManager(void)
{
}


QueryManager::~QueryManager(void)
{
}

QueryManager* QueryManager::getInstance(void)
{
	if (queryManager==NULL)
	{
		queryManager = new QueryManager();
	}
	return queryManager;
}
void QueryManager::registerStream(boost::shared_ptr<IStreamInput> streamInput)
{
	this->registeredStreamMap.insert(make_pair(streamInput->getId(),streamInput));
}
boost::shared_ptr<IStreamInput>  QueryManager::getRegisteredStreamById(std::string id)
{
 	std::map<std::string, boost::shared_ptr<IStreamInput> >::iterator it;
	it = registeredStreamMap.find(id);
	assert(it!=registeredStreamMap.end());
	
	return it->second;
		
}


boost::shared_ptr<QueryEntity> QueryManager::addQuery(boost::shared_ptr<QueryIntermediateRepresentation>queryIntermediateRepresentation,boost::shared_ptr<IStreamOutput> streamOutput, std::string queryDuration)
{
	boost::shared_ptr<QueryEntity>queryEntity(new QueryEntity(queryIntermediateRepresentation,streamOutput,queryDuration));
	this->queryEntityList.push_back(queryEntity);
	return queryEntity;
}

void QueryManager::registerRelation(
		boost::shared_ptr<RelationInput> relationInput) {
	this->registeredRelationMap.insert(
			make_pair(relationInput->getId(), relationInput));
}
boost::shared_ptr<RelationInput> QueryManager::getRegisteredRelationById(
		std::string id) {
	std::map<std::string, boost::shared_ptr<RelationInput> >::iterator it;
	it = registeredRelationMap.find(id);
	assert(it != registeredStreamMap.end());

	return it->second;
}


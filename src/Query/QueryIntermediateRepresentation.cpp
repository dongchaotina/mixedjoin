#include "../Common/stdafx.h"
#include "../Query/QueryIntermediateRepresentation.h"
#include "../Common/Types.h"

QueryIntermediateRepresentation::QueryIntermediateRepresentation(Document intermediateRepDocument)
{
	this->intermediateRepDocument = intermediateRepDocument;
}


QueryIntermediateRepresentation::~QueryIntermediateRepresentation(void)
{
}

Document& QueryIntermediateRepresentation::getDocument()
{
	return this->intermediateRepDocument;
}
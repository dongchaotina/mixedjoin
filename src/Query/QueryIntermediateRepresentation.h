#pragma once
#include "../Common/stdafx.h"
#include "../Common/Types.h"
// this is explained by QueryIntermediateRepresentationInterpreter
class QueryIntermediateRepresentation
{
private:
	Document intermediateRepDocument;
public:
	QueryIntermediateRepresentation(Document intermediateRepDocument);
	~QueryIntermediateRepresentation(void);
	Document& getDocument(void);
};


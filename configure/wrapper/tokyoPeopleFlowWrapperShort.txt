{
"implement_class":"TokyoPeopleFlowStreamInput_short",
"class_argument":{
    "data_folder":["/root"]
    },
"information_source_schema":    {
                                    "id":"tokyoPeopleFlowStreamShort",
                                    "type":"object", "properties":
                                        {
                                                                    "pid":{"type": "string"},
                                                                    "longitude":{"type": "float"},
                                                                    "latitude":{"type": "float"},
                                                                    "sex":{"type": "int"},
                                                                    "age":{"type": "int"},
                                                                    "work":{"type": "int"}
                                        }
                                }
}

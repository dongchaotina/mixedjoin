# README #



### Library Installation ###

* Boost
    A boost library with a version >= 1.49 is strongly recommanded.
* libevent
* curl

### How do I get set up? ###

Execution
(1)
run the JsSpinner server.
In the jsonstream folder. 
~$: ./make
~$: ./main.exe
(2)
Run the JAVA Parser Server
Use eclipse to load the project in the jsonstreamparser folder.
Execute two classes.
one is run.java
one is StreamSchemaServer.java    // this class would be removed here at last
(3)
Run the client program to register queries.
In the jsonstreamclient folder.
~$: ./make
~$: ./main.exe